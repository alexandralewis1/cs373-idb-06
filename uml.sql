//// -- LEVEL 1
//// -- Schemas, Tables and References

// Creating tables
// You can define the tables with full schema names

Table Arenas as A {
  aid int [ref: - T.aid]
  active bool
  arenaname text
  address text
  city text
  state text
  zip text
  country text
  capacity text
  lat int
  long int
  tid int
  image text [ref: - T.logo]

   Indexes {
    (aid, city, image) [pk]
  }
}

Table Teams as T {
  tid int [ref: - P.tid]
  abbreviation text [ref: - P.teamshort]
  active bool
  city text [ref: - A.city]
  teamname text
  aid int
  conference text
  division text
  primarycolor text
  secondarycolor text
  tertiarycolor text
  quaternarycolor text
  logo text
  nbadotcomteamid int
  twitter text

  Indexes {
    (aid, tid, city, logo, abbreviation) [pk]
  }
}

Table Players as P {
  id int
  tid int
  teamshort text
  jersey int
  position text
  first text
  last text
  height int
  weight int
  birthday text
  city text
  country text
  college text
  salary int
  photo text
  experience int
  twitter text

  Indexes {
    (id, tid, teamshort) [pk]
  }
}




// If schema name is omitted, it will default to "public" schema.


// Table countries {
//   code int [pk]
//   name varchar
//   continent_name varchar
//  }

// // Creating references
// // You can also define relaionship separately
// // > many-to-one; < one-to-many; - one-to-one; <> many-to-many
// Ref: U.country_code > countries.code  
// Ref: ecommerce.merchants.country_code > countries.code

// //----------------------------------------------//

// //// -- LEVEL 2
// //// -- Adding column settings

// Table ecommerce.order_items {
//   order_id int [ref: > ecommerce.orders.id] // inline relationship (many-to-one)
//   product_id int
//   quantity int [default: 1] // default value
// }

// Ref: ecommerce.order_items.product_id > ecommerce.products.id

// Table ecommerce.orders {
//   id int [pk] // primary key
//   user_id int [not null, unique]
//   status varchar
//   created_at varchar [note: 'When order created'] // add column note
// }

// //----------------------------------------------//

// //// -- Level 3 
// //// -- Enum, Indexes

// // Enum for 'products' table below
// Enum ecommerce.products_status {
//   out_of_stock
//   in_stock
//   running_low [note: 'less than 20'] // add column note
// }

// // Indexes: You can define a single or multi-column index 
// Table ecommerce.products {
//   id int [pk]
//   name varchar
//   merchant_id int [not null]
//   price int
//   status ecommerce.products_status
//   created_at datetime [default: `now()`]
  
//   Indexes {
//     (merchant_id, status) [name:'product_status']
//     id [unique]
//   }
// }

// Table ecommerce.product_tags {
//   id int [pk]
//   name varchar
// }

// Table ecommerce.merchant_periods {
//   id int [pk]
//   merchant_id int
//   country_code int
//   start_date datetime
//   end_date datetime
// }

// Ref: ecommerce.products.merchant_id > ecommerce.merchants.id // many-to-one
// Ref: ecommerce.product_tags.id <> ecommerce.products.id // many-to-many
// //composite foreign key
// Ref: ecommerce.merchant_periods.(merchant_id, country_code) > ecommerce.merchants.(id, country_code)
