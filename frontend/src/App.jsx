import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import Landing from './pages/landing';
import About from './pages/about';
import Players from './pages/players';
import Player from './pages/player';
import Teams from './pages/teams';
import Team from './pages/team';
import Arenas from './pages/arenas';
import Arena from './pages/arena';
import Visualizations from './pages/visualizations';
import Search from './pages/search';
import WebsiteNavbar from './components/Navbar';
import ProviderVisualizations from './pages/providerVisualizations';

function App() {
  return (
    <div>
      <WebsiteNavbar />
      <Router>
        <Routes>
          <Route path="/" element={<Landing />} />
          <Route path="/about" element={<About />} />
          <Route path="/players" element={<Players />} />
          <Route path="/player/:id" element={<Player />} />
          <Route path="/teams" element={<Teams />} />
          <Route path="/team/:id" element={<Team />} />
          <Route path="/arenas" element={<Arenas />} />
          <Route path="/arena/:id" element={<Arena />} />
          <Route path="/visualizations" element={<Visualizations />} />
          <Route path="/providervisualizations" element={<ProviderVisualizations />} />
          <Route path="/search/:query" element={<Search />} />
        </Routes>
      </Router>
    </div>
  );
}

export default App;
