import amplifyImg from '../assets/tools-pictures/amplify.jpg';
import axiosImg from '../assets/tools-pictures/axios.jpg';
import discordImg from '../assets/tools-pictures/discord.jpg';
import gitlabImg from '../assets/tools-pictures/gitlab.jpg';
import namecheapImg from '../assets/tools-pictures/namecheap.jpg';
import npmImg from '../assets/tools-pictures/npm.jpg';
import postmanImg from '../assets/tools-pictures/postman.jpg';
import reactImg from '../assets/tools-pictures/react.jpg';
import viteImg from '../assets/tools-pictures/vite.jpg';
import vscodeImg from '../assets/tools-pictures/vscode.jpg';
import zoomImg from '../assets/tools-pictures/zoom.jpg';
import selImg from '../assets/tools-pictures/selenium.jpg';
import rdsImg from '../assets/tools-pictures/rds.jpg';
const toolInfo = [
  {
    name: 'React',
    link: 'https://reactjs.org/docs/getting-started.html',
    image: reactImg,
    description: 'JavaScript library for building UIs',
  },
  {
    name: 'React-Bootstrap',
    link: 'https://react-bootstrap.github.io/getting-started/introduction',
    image: reactImg,
    description: 'Front-end framework for responsive web apps',
  },
  {
    name: 'React Router',
    link: 'https://reactrouter.com/en/main',
    image: reactImg,
    description: 'Library for routing in React',
  },
  {
    name: 'NameCheap',
    link: 'https://nc.me/',
    image: namecheapImg,
    description: 'Domain name registrar',
  },
  {
    name: 'AWS Amplify',
    link: 'https://aws.amazon.com/amplify/?trk=66d9071f-eec2-471d-9fc0-c374dbda114d&sc_channel=ps&s_kwcid=AL!4422!3!646025317188!e!!g!!aws%20amplify&ef_id=Cj0KCQiAorKfBhC0ARIsAHDzslvszE_lMkagxt6U5tjCv3lIZLery0CZ_MqqYJ-RdgSINn_EjAsQjV0aAj7REALw_wcB:G:s&s_kwcid=AL!4422!3!646025317188!e!!g!!aws%20amplify/',
    image: amplifyImg,
    description: 'Cloud platform for hosting web apps',
  },
  {
    name: 'Discord',
    link: 'https://discord.com/',
    image: discordImg,
    description: 'Online communication platform',
  },
  {
    name: 'Visual Studio Code',
    link: 'https://code.visualstudio.com/',
    image: vscodeImg,
    description: 'Code editor',
  },
  {
    name: 'Zoom',
    link: 'https://zoom.us/',
    image: zoomImg,
    description: 'Video conferencing platform',
  },
  {
    name: 'GitLab',
    link: 'https://about.gitlab.com/',
    image: gitlabImg,
    description: 'Git repository manager',
  },

  {
    name: 'Postman',
    link: 'https://www.postman.com/product/what-is-postman/',
    image: postmanImg,
    description: 'API platform for building and testing APIs',
  },
  {
    name: 'Axios',
    link: 'https://axios-http.com/docs/intro',
    image: axiosImg,
    description: 'JavaScript library for making HTTP requests',
  },
  {
    name: 'NPM',
    link: 'https://docs.npmjs.com/',
    image: npmImg,
    description: 'Package manager for Node.js',
  },
  {
    name: 'ViteJS',
    link: 'https://vitejs.dev/',
    image: viteImg,
    description: 'Fast build tool for modern web development',
  },

  {
    name: 'Selenium',
    link: 'https://www.selenium.dev/documentation/',
    image: selImg,
    description: 'A tool used to write functional tests for the GUI',
  },
   
  {
    name: 'Amazon RDS',
    link: 'https://aws.amazon.com/rds/',
    image: rdsImg,
    description: 'Launched a Postgres database on RDS to store the data remotely.',
  },



];

export { toolInfo };
