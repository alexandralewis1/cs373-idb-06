import rapidImg from '../assets/api-pictures/rapid.jpg';
import sportdataImg from '../assets/api-pictures/sportsdata.jpg';
import ticketmasterImg from '../assets/api-pictures/ticketmaster.jpg';
import wikipediaImg from '../assets/api-pictures/wikipedia.jpg';
const apiInfo = [
  {
    name: 'SportsData API',
    link: 'https://sportsdata.io/nba-api',
    image: sportdataImg,
    description: 'Gives information about NBA players',
  },
  {
    name: 'Rapid API',
    link: 'https://rapidapi.com/api-sports/api/api-nba/',
    image: rapidImg,
    description: 'Gives information about NBA teams',
  },
  {
    name: 'TicketMaster API',
    link: 'https://developer.ticketmaster.com/products-and-docs/apis/getting-started/',
    image: ticketmasterImg,
    description: 'Gives information about stadiums',
  },
  {
    name: 'Wikipedia API',
    link: 'https://en.wikipedia.org/api/rest_v1/',
    image: wikipediaImg,
    description: 'Used to get images and blurbs',
  },
];

export { apiInfo };
