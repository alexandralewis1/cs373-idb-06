import AyushPic from '../assets/profile-pictures/ayush.png';
import LexiPic from '../assets/profile-pictures/lexi.jpg';
import AdityaPic from '../assets/profile-pictures/aditya.jpeg';
import KellyPic from '../assets/profile-pictures/kelly.jpg';
import MasonPic from '../assets/profile-pictures/mason.jpg';

// inspired by GeoJobs project. https://gitlab.com/sarthaksirotiya/cs373-idb/-/blob/81897f41356a65e96c3a9aea75b2c16ab0fe5221/front-end/src/static/TeamInfo.jsx

const teamInfo = [
  {
    name: 'Ayush Patel',
    gitlab_username: 'AyushPatel101',
    email: 'patayush01@gmail.com',
    image: AyushPic,
    role: 'Full-Stack',
    bio: "I'm a senior CS major at the University of Texas at Austin. In my free time I enjoy skateboarding, playing sports, and watching Netflix. ",
    commits: 0,
    issues: 0,
    unit_tests: 10,
  },
  {
    name: 'Lexi Lewis',
    gitlab_username: 'alexandralewis1',
    email: 'alexandralewis@utexas.edu',
    image: LexiPic,
    role: 'Front end',
    bio: "I'm a junior CS major at UT! I love to spend time with friends & listen to music.",
    commits: 0,
    issues: 0,
    unit_tests: 0,
  },
  {
    name: 'Aditya Agrawal',
    gitlab_username: 'DaSniper12',
    email: 'adityaagrawal200313@gmail.com',
    image: AdityaPic,
    role: 'DevOps',
    bio: "I'm a sophmore CS major at UT. I love to play golf and try out new places to eat in my free time.",
    commits: 0,
    issues: 0,
    unit_tests: 20,
  },
  {
    name: 'Kelly Thai',
    gitlab_username: 'kellythai02',
    email: 'kellythai@utexas.edu',
    image: KellyPic,
    role: 'Front End',
    bio: 'I am a junior CS major at UT Austin! I like to watch TV, read, and hang out with friends in my free time.',
    commits: 0,
    issues: 0,
    unit_tests: 0,
  },
  {
    name: 'Mason Wilderom',
    gitlab_username: 'wilderom',
    email: 'mpw@cs.utexas.edu',
    image: MasonPic,
    role: 'Backend',
    bio: "I'm a junior CS major here at UT. I grew up in Dallas and enjoy golfing. In my free time I like to go downtown with friends.",
    commits: 0,
    issues: 0,
    unit_tests: 0,
  },
];

export { teamInfo };
