import {Container} from 'react-bootstrap'
import Typography from "@mui/material/Typography";
import ProviderPlayerPerSport from "../components/visualizations/ProviderPlayerPerSport";
import ProviderAveragePlayerHeightPerLeague from "../components/visualizations/ProviderAveragePlayerHeightPerLeague";
import ProviderAverageLeaguePerCountry from "../components/visualizations/ProviderAverageLeaguePerCountry";

const ProviderVisualizations = () => {
    return (
        <Container>
            <Typography
            variant="h3"
            sx={{ textAlign: "center" }}
            style={{
                padding: "45px"
            }}>
                Provider Visualizations
            </Typography>
            <Typography
            variant="h5"
            sx={{ textAlign: "center" }}
            style={{
                padding: "15px"
            }}>
                # of Players per Sport
            </Typography>
            <ProviderPlayerPerSport />
            <Typography
            variant="h5"
            sx={{ textAlign: "center" }}
            style={{
                padding: "15px"
            }}>
                Average Player Height per League
            </Typography>
            <ProviderAveragePlayerHeightPerLeague/>
            <Typography
            variant="h5"
            sx={{ textAlign: "center" }}
            style={{
                padding: "15px"
            }}>
                Number of Leagues per Country
            </Typography>
            <ProviderAverageLeaguePerCountry/>
        </Container>
    )
}

export default ProviderVisualizations;