import { useState, useEffect } from 'react';
import axios from 'axios';
import 'bootswatch/dist/lux/bootstrap.min.css';

import AboutCard from '../components/AboutCard';
import APICard from '../components/APICard';
import { teamInfo } from '../static/AboutInfo';
import { apiInfo } from '../static/APIInfo';
import { toolInfo } from '../static/ToolInfo';

import Spinner from 'react-bootstrap/Spinner';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
import Stack from 'react-bootstrap/Stack';
import Grid from '@mui/material/Grid';

const client = axios.create({
  baseURL: 'https://gitlab.com/api/v4/',
  headers: { Authorization: 'Bearer glpat-LUeTABZFAxxRytZKsibU' },
});

const fetchGitLabData = async () => {
  let totalCommits = 0,
  totalIssues = 0,
  totalUnitTests = 0;

for (const member of teamInfo) {
member.commits = 0;
member.issues = 0;
totalUnitTests += member.unit_tests;
const response = await client.get(`projects/43434689/issues_statistics?assignee_username=${member.gitlab_username}`);
member.issues = response.data['statistics']['counts']['all'];
totalIssues += member.issues;

// Wait for this member's commits to be fetched
const contributorResponse = await client.get('projects/43434689/repository/contributors');
const uniqueElements = [];
for (const element of contributorResponse.data) {
  const { name, email, commits } = element;
  const index = uniqueElements.findIndex((el) => el.name === element.name);
  if (index === -1) {
    uniqueElements.push(element);
  } else {
    uniqueElements[index].commits += element.commits;
  }
}
for (const element of uniqueElements) {
  const { name, email, commits } = element;
  if (member.name === name || member.gitlab_username === name || member.email === email) {
    member.commits = commits;
    totalCommits += commits;
  }
}
}

for (const member of teamInfo) {
console.log(member.issues);
console.log(member.commits)
}

return {
totalCommits: totalCommits,
totalIssues: totalIssues,
totalTests: totalUnitTests,
teamInfo: teamInfo,
};
};

const About = () => {
  const [teamList, setTeamList] = useState([]);
  const [totalCommits, setTotalCommits] = useState(0);
  const [totalIssues, setTotalIssues] = useState(0);
  const [totalTests, setTotalTests] = useState(0);
  const [loaded, setLoaded] = useState(false);

  useEffect(() => {
    const fetchData = async () => {
      if (teamList === undefined || teamList.length === 0) {
        const gitlabInfo = await fetchGitLabData();
        setTotalCommits(gitlabInfo.totalCommits);
        setTotalIssues(gitlabInfo.totalIssues);
        setTotalTests(gitlabInfo.totalTests);
        setTeamList(gitlabInfo.teamInfo);
        setLoaded(true);
      }
    };
    fetchData();
  }, [teamList]);

  return (
    <Stack>
      <Container className="p-4">
        <h1 className="d-flex justify-content-center">What is NBADB?</h1>
        <p>
          NBADB is designed to connect NBA fans to information about their favorite players, teams, and the arenas where
          the games are played. Becoming a superfan is as easy as clicking on the various pages, so get started now!,
        </p>
        <h1 className="d-flex justify-content-center">A One Stop Shop</h1>
        <p>
          Wondering how your favorite players are doing? What team they are on? Where they play? NBADB connects you to
          everything you may want or need to know about the NBA in one convenient place.
        </p>
      </Container>
      <Grid item md={5}>
        <h1 className="d-flex justify-content-center">Meet the Team!</h1>
        {loaded ? (
          <Row>
            {teamList.map((member) => {
              return (
                <Col key={member.name} className="d-flex align-self-stretch">
                  <AboutCard devInfo={member} />
                </Col>
              );
            })}
          </Row>
        ) : (
          <Row>
            <Col className="d-flex justify-content-center">
              <Spinner animation="grow" />
            </Col>
          </Row>
        )}
      </Grid>
      <Container className="p-4">
        <h1 className="d-flex justify-content-center">Total Repository Stats</h1>
        <Row>
          <Col className="d-flex justify-content-center">Total Commits: {totalCommits}</Col>
          <Col className="d-flex justify-content-center">Total Issues: {totalIssues}</Col>
          <Col className="d-flex justify-content-center">Total Unit Tests: {totalTests}</Col>
        </Row>
      </Container>
      <Container className="p-4">
        <a href="https://documenter.getpostman.com/view/25706553/2s935uGgU5">
          <h3 className="d-flex justify-content-center">
            <u>Postman Documentation</u>
          </h3>
        </a>
      </Container>
      <Container className="text-center mt-5">
        <h1>APIs</h1>
        <center>
          <Row
            // xs={1}
            // sm={2}
            // md={3}
            // xl={3}
            className="g-4 p-4 justify-content-center"
          >
            {apiInfo.map((apiInfoEntry) => {
              return (
                <Col className="d-flex align-self-stretch" key={apiInfoEntry.name}>
                  <APICard info={apiInfoEntry} />
                </Col>
              );
            })}
          </Row>
        </center>
      </Container>
      <Container className="text-center mt-5">
        <h1>Tools</h1>
        <center>
          <Row
            // xs={1}
            // sm={2}
            // md={3}
            // xl={3}
            className="g-4 p-4 justify-content-center"
          >
            {toolInfo.map((toolInfoEntry) => {
              return (
                <Col className="d-flex align-self-stretch" key={toolInfoEntry.name}>
                  <APICard info={toolInfoEntry} />
                </Col>
              );
            })}
          </Row>
        </center>
      </Container>
    </Stack>
  );
};

export default About;
