import 'bootswatch/dist/lux/bootstrap.min.css';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import ArenaCard from '../components/ArenaCard';
// import { Stack } from 'react-bootstrap';
// import Arena from './arena';
import axios from 'axios';
import { useState, useEffect } from 'react';
import Pagination from 'react-bootstrap/Pagination';
import Spinner from 'react-bootstrap/Spinner';
import { Container, Form, Button } from 'react-bootstrap';
import Sidebar from '../components/Sidebar';

const client = axios.create({ baseURL: import.meta.env.VITE_API_URL });

/**
 * Displays all the arena cards
 */
const Arenas = () => {
  // Initialize empty array of arenas
  const [currentArenas, setCurrentArenas] = useState([]);
  // Number of teams return 
  const [count, setCount] = useState(0);
  // We have not yet loaded the data
  const [loaded, setLoaded] = useState(false);
  // What page we are on
  const [currentPage, setCurrentPage] = useState(2);
  // All arenas - loaded for filtering purposes 
  const [allArenas, setAllArenas] = useState([]);
  const [allLoad, setAllLoad] = useState(false);
  // items per page 
  const ITEMS_PER_PAGE = 20;
  // items returned 
  const [itemsReturned, setItemsReturned] = useState(ITEMS_PER_PAGE);

  /**
   * Parameters for filteirng 
   */
  const [city, setCity] = useState("All");
  const [country, setCountry] = useState("All")
  // const [active, setActive] = useState("All")
  const [arenaname, setName] = useState("All")
  const [state, setState] = useState("All")
  const [capacity, setCapacity] = useState("All")
  // Create the map for the filter bar 
  let categories = new Map([
    // ['arenaname', 'Name'],
    ['city', 'City'],
    ['state', 'State'],
    ['country', 'Country'],
    // ['active', 'Active'],
    ['capacity', 'Capacity']
  ]);

  let sortCategories =  {
    'Name': 'arenaname',
    'Capacity': 'capacity',
  }
  
  /**
   * parameters for sorting 
   */
  const [selectedOption, setSelectedOption] = useState("arenaname")

  /**
   * parameters for querying 
   * @param {} pageNumber 
   */
  const [searchBarInput, setSearchbarInput] = useState("")
  const [searchQuery, setSearchQuery] = useState("")

  function pageChange(pageNumber) {
    setCurrentPage(pageNumber);
    setLoaded(false);
  }

  /**
   * Handles a filter call
   * @param {*} filterCategory: string indicating the category to be filtered 
   * @param {*} selection: either a string with the name or an array with [min, max]
   */
  const handleFilterChange = (filterCategory,
    selection = null) => {
    if (filterCategory == "City") {
      setCity(selection);
    }
    if (filterCategory == "Country") {
      setCountry(selection)
    }
    // if (filterCategory == "Active") {
    //   setActive(selection)
    // }
    if (filterCategory == "Name") {
      setName(selection)
    }
    if (filterCategory == "State") {
      setState(selection)
    }
    if (filterCategory == "Capacity") {
      setCapacity(selection)
    }
    setLoaded(false);
  }

  /**
  * Handles the sort 
  * @param {} sortCategory 
  * @param {*} isAscending 
  */
  const handleSort = async (e) => {
    setSelectedOption(e.target.value);
    setLoaded(false)
  }

  /**
 * Handles a query 
*/
  const handleSearch = (e) => {
    console.log("Search")
    e.preventDefault()

    setSearchQuery(searchBarInput.replace(" ", "+"))
    console.log(searchQuery)

    setLoaded(false)
  }

  useEffect(() => {
    // Read arenas from database
    const fetchArenas = async () => {
      let query = "v1/json/arenas?offset=" + (currentPage - 1) + "&limit=" + ITEMS_PER_PAGE + "&sort=" + selectedOption
      if (searchQuery != "All" || searchQuery != "") {
        query += "&search=" + searchQuery
      } 
      if (arenaname != "All") {
        query += "&name=" + arenaname
      }
      if (state != "All") {
        query += "&state=" + state
      }
      if (city != "All") {
        query += "&city=" + city
      }
      if (country != "All") {
        query += "&country=" + country
      }
      // if (active != "All") {
      //   query += "&active=" + active
      // }
      if (capacity != "All") {
        query += "&capacity=1&min=" + capacity[0] +"&max=" + capacity[1]
      }
    
      
     
      /**
       * TODO: add the filters and sorting and searchy query manipulation logic 
       */
      console.log(query)
      await client
        .get(query, {
          headers: {
            'Access-Control-Allow-Origin': '*',
          },
        })
        .then((response) => {
          setItemsReturned(response.data.data.length);
          setCount(response.data.total);
          setCurrentArenas(response.data);
        });
      setLoaded(true);
    };
    const fetchAll = async () => {
      await client
        .get('v1/json/arenas', {
          headers: {
            'Access-Control-Allow-Origin': '*',
          },
        })
        .then((response) => {
          setAllArenas(response.data);
          setAllLoad(true);
        });
    };
    // Load the arenas if no done yet
    if (!loaded) {
      fetchArenas();
    }
    if (!allLoad) {
      fetchAll();
    }
  });

  // Create the pagination bar - since doing paginated queries dont know how many pages to make (hard code math?)
  const numPages = (count % ITEMS_PER_PAGE) == 0 ? (count / ITEMS_PER_PAGE) : (Math.floor(count / ITEMS_PER_PAGE) + 1);
  const items = [];
  for (let i = 1; i <= numPages; i++) {
    items.push(
      <Pagination.Item key={i} onClick={() => pageChange(i + 1)}>
        {i}
      </Pagination.Item>,
    );
  }

  return (
    // Returns a grid of cards
    <Container>
      <Row>
        <h1> Arenas </h1>
        <Form onSubmit={handleSearch}>
          <Form.Control 
          type="search" 
          placeholder="Search arenas"
          value={searchBarInput}
          onChange={(e) => setSearchbarInput(e.target.value)}></Form.Control> <br></br>
          <br></br>
          <Button type="submit"> Submit </Button>
        </Form>
      </Row>
      <Row>
        <Row>
          <Col></Col>
          <Col></Col>
          <Col>
            <Form.Select
            onChange={handleSort}
            value={selectedOption}>
              <option value = "arenaname"> Sort By Name: A-Z </option>
              <option value="capacity">Sort By Capacity: Low to High</option>
            </Form.Select> <br />
          </Col>
        </Row>
        <Col xs={2}>
          {allLoad ? (<Sidebar data={allArenas} categories={categories} handleFilterChange={handleFilterChange} />) : <Spinner animation="grow" />}
        </Col>
        <Col xs={10}>
          <Row xl={4}
            lg={3}
            md={2}
            sm={1}
            xs={1}>
            {loaded ? (
              currentArenas['data'].map((arena) => {
                return (
                  // Send arena data to the arena card object
                  <Col key={arena.id} className="d-flex align-self-stretch" style={{minWidth:"225px"}}>
                    <ArenaCard arena={arena} highlight={searchQuery}/>
                  </Col>
                );
              })
            ) : (
              <Spinner animation="grow" />
            )}
          </Row>
        </Col>
      </Row>
      <Row>
        <Pagination className="justify-content-center">{items}</Pagination>
        <h9>Page {currentPage - 1} / {numPages}{' '} <br /> </h9>
        <h9>Showing {itemsReturned} of {count} instances </h9>
      </Row>
    </Container>
  );
};

export default Arenas;
