import 'bootswatch/dist/lux/bootstrap.min.css';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import PlayerCard from '../components/PlayerCard';
import Sidebar from '../components/Sidebar';
// import { Stack } from 'react-bootstrap';
// import Player from './player';
import axios from 'axios';
import { useState, useEffect } from 'react';
import Pagination from 'react-bootstrap/Pagination';
import Spinner from 'react-bootstrap/Spinner';
import Form from 'react-bootstrap/Form';
import { Container, Button } from 'react-bootstrap';

const client = axios.create({ baseURL: import.meta.env.VITE_API_URL });

/**
 * Displays all the player cards
 */
const Players = () => {
  /**
   * Data for showing the cards 
   */
  const [data, setData] = useState([]);
  // Initialize empty array of players
  const [currentPlayers, setCurrentPlayers] = useState([]);
  // Number of teams return 
  const [count, setCount] = useState(0);
  // We have not yet loaded the data
  const [loaded, setLoaded] = useState(false);
  // What page we are on
  const [currentPage, setCurrentPage] = useState(2);
  // All players - loaded for filtering purposes 
  const [allPlayers, setAllPlayers] = useState([]);
  const [allLoad, setAllLoad] = useState(false);
  // items per page 
  const ITEMS_PER_PAGE = 20;
  // items returned 
  const [itemsReturned, setItemsReturned] = useState(ITEMS_PER_PAGE);

  /**
   * Parameters for filteirng 
   */
  const [team, setTeam] = useState("All");
  const [position, setPosition] = useState("All")
  const [country, setCountry] = useState("All")
  const [salary, setSalary] = useState("All")
  // Create the map for the filter bar 
  let categories = new Map([
    ['teamshort', 'Team'],
    ['position', 'Position'],
    ['country', 'Country'],
    ['salary', 'Salary']
  ]);

  /**
   * parameters for sorting 
   */
  const [selectedOption, setSelectedOption] = useState("first")

  /**
   * parameters for querying 
   * @param {} pageNumber 
   */
  const [searchBarInput, setSearchbarInput] = useState("")
  const [searchQuery, setSearchQuery] = useState("")

  // function that changes the page 
  function pageChange(pageNumber) {
    setCurrentPage(pageNumber);
    setLoaded(false);
  }

  /**
   * Handles a filter call
   * @param {*} filterCategory: string indicating the category to be filtered 
   * @param {*} selection: either a string with the name or an array with [min, max]
   */
  const handleFilterChange = (filterCategory, 
                              selection = null) => {
    if (filterCategory == "Team") {
      setTeam(selection);
    }
    if (filterCategory == "Position") {
      setPosition(selection)
    }
    if (filterCategory == "Country") {
      setCountry(selection)
    }
    if (filterCategory == "Salary") {
      setSalary(selection)
    }
    setLoaded(false);
  }

  /**
   * Handles the sort 
   */
  const handleSort = async (event) => {
    setSelectedOption(event.target.value);
    
    setLoaded(false);
  };

    /**
     * Handles a query 
   */
    const handleSearch = (e) => {
      console.log("Search")
      e.preventDefault()

      setSearchQuery(searchBarInput.replace(" ", "+"))
      console.log(searchQuery)

      setLoaded(false)
    }

  // On load 
  useEffect(() => {
    // Read players from database
    const fetchPlayers = async () => {
      let query = "v1/json/players?offset=" + (currentPage - 1) + "&limit=" + ITEMS_PER_PAGE + "&sort=" + selectedOption
      
      if (team != "All") {
        query += "&teamshort=" + team
      }
      if (position != "All") {
        query += "&position=" + position
      }
      if (country != "All") {
        query += "&country=" + country
      }
      if (salary != "All") {
        query += "&salary=1&min=" + salary[0] +"&max=" + salary[1]
      }
      if (searchQuery != "") {
        query += "&search=" + searchQuery
      }
      /**
       * TODO: Add the country and salary filters; add searching; add sorting 
       */
      console.log(query)
      await client
        .get(query, {
          headers: {
            'Access-Control-Allow-Origin': '*',
          },
        })
        .then((response) => {
          console.log(response.data)
          setItemsReturned(response.data.data.length);
          setCount(response.data.total);
          setCurrentPlayers(response.data);
        });
      setLoaded(true);
    };
    const fetchAll = async () => {
      await client
      .get('v1/json/players', {
        headers: {
          'Access-Control-Allow-Origin': '*',
        },
      })
        .then((response) => {
          setAllPlayers(response.data);
          console.log("all ", allPlayers)
          setAllLoad(true);
        });
    };
    // Load the players if no done yet
    if (!loaded) {
      fetchPlayers();
    } 
    if (!allLoad) {
      fetchAll();
    }
  });

  // Create the pagination bar - since doing paginated queries dont know how many pages to make (hard code math?)
  const numPages = (count % ITEMS_PER_PAGE) == 0 ? (count / ITEMS_PER_PAGE) : (Math.floor(count / ITEMS_PER_PAGE) + 1);
  const items = [];
  for (let i = 1; i <= numPages; i++) {
    items.push(
      <Pagination.Item key={i} onClick={() => pageChange(i + 1)}>
        {i}
      </Pagination.Item>,
    );
  }

  return (
    // Returns a grid of cards
    <Container>
      <Row>
        <h1>Players</h1>
        <Form  onSubmit={handleSearch}>
          <Form.Control 
          type="search" 
          placeholder="Search Players" 
          value={searchBarInput} 
          onChange={(e) => setSearchbarInput(e.target.value)} />
          <br></br>
          <Button type="submit"> Submit </Button>
        </Form>
      </Row>
      <Row>
        <Row>
          <Col></Col>
          <Col></Col>
          <Col>
          <Form.Select 
          onChange={handleSort} 
          value={selectedOption}>
            <option value ="first"> Sort By First Name: A-Z </option>
            <option value ="last"> Sort By Last Name: A-Z </option>
            <option value="height">Sort By Height: Low to High</option>
            <option value="salary">Sort By Salary: Low to High</option>
          </Form.Select> <br/> 
          </Col>       
        </Row>
        <Col xs={2}>
          {allLoad ? (<Sidebar data={allPlayers} categories={categories} handleFilterChange={handleFilterChange}/>) : <Spinner animation="grow" /> }
        </Col>
        <Col xs={10}>
          <Row xl={4}
            lg={3}
            md={2}
            sm={1}
            xs={1} >
            {loaded ? (
              currentPlayers['data'].map((player) => {
                return (
                  // Send player data to the player card object
                  <Col key={player.id} className="d-flex align-self-stretch" style={{minWidth:"225px"}}>
                    <PlayerCard player={player} highlight={searchQuery}/>
                  </Col>
                );
              })
            ) : (
              <Spinner animation="grow" />
            )}
          </Row>
        </Col>
      </Row>
      <Row>
        <Pagination className="justify-content-center">{items}</Pagination>
        <h9>Page {currentPage - 1} / {numPages}{' '} <br /> </h9>
        <h9>Showing {itemsReturned} of {count} instances </h9>
      </Row>
    </Container>
  );
};

export default Players;
