import Container from "react-bootstrap/Container";
import PlayersPerCountry from "../components/visualizations/PlayersPerCountry"
import AverageSalaryPerTeam from "../components/visualizations/AverageSalaryPerTeam"
import CapacityPerSpending from "../components/visualizations/ArenaCapacityPerTeamSpending"
import Typography from "@mui/material/Typography";


const Visualizations = () => {
    return (
        <Container>
            <Typography
            variant="h3"
            sx={{ textAlign: "center" }}
            style={{
                padding: "15px"
            }}>
                Visualizations
                
            </Typography>
            {<CapacityPerSpending> </CapacityPerSpending>}
            {<AverageSalaryPerTeam></AverageSalaryPerTeam>}
            { <PlayersPerCountry></PlayersPerCountry>}
        </Container>
    )
    
}

export default Visualizations;