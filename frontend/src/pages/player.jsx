import { useParams, Link } from 'react-router-dom';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Image from 'react-bootstrap/Image';
import { Timeline } from 'react-twitter-widgets';
import axios from 'axios';
import { useState, useEffect } from 'react';
import {Spinner, Accordion } from 'react-bootstrap';

const client = axios.create({ baseURL: import.meta.env.VITE_API_URL });

function inchesToFeetAndInches(inches) {
  const feet = Math.floor(inches / 12);
  const remainingInches = inches % 12;
  const feetString = `${feet}'`;
  const inchesString = `${remainingInches}"` ;
  return `${feetString}${inchesString}`;
}

/**
 * This class contains the extended information for any given player
 */
const Player = () => {
  const [player, setPlayer] = useState([]);
  const [loaded, setLoaded] = useState(false);
  const { id } = useParams();

  const [relatedTeam, setRelatedTeam] = useState([]);
  const[loadedTeam, setLoadedTeam] = useState(false);

  const [relatedArena, setRelatedArena] = useState([]);
  const [loadedArena, setLoadedArena] = useState(false);

  // fetch the player 
  useEffect(() => {
    const fetchPlayer = async () => {
      await client
        .get('v1/json/players/' + id, {
          headers: {
            'Access-Control-Allow-Origin': '*',
          },
        })
        .then((response) => {
          console.log(response.data.data[0]);
          setPlayer(response.data.data[0]);
        });
      setLoaded(true);
    };
    if (!loaded) {
      fetchPlayer();
    }
  });

  // fetch the related team 
  useEffect(() => {
    const fetchTeam = async () => {
      await client
        .get('v1/json/teams/' + teamid, {
          headers: {
            'Access-Control-Allow-Origin': '*',
          },
        })
        .then((response) => {
          setRelatedTeam(response.data.data[0]);
          console.log("team");
          console.log(response.data.data[0]);
        });
      setLoadedTeam(true);
    };
    if (loaded && !loadedTeam) {
      fetchTeam();
    }
  });

  // fetch the related arena 
  useEffect(() => {
    const fetchTeam = async () => {
      await client
        .get('v1/json/arenas/' + relatedTeam.aid, {
          headers: {
            'Access-Control-Allow-Origin': '*',
          },
        })
        .then((response) => {
          setRelatedArena(response.data.data[0]);
          console.log("arena");
          console.log(response.data.data[0]);
        });
      setLoadedArena(true);
    };
    if (loaded && loadedTeam && !loadedArena) {
      fetchTeam();
    }
  });

  const name = player?.first + ' ' + player?.last;
  const picture = player?.photo;
  const team = player?.teamshort;
  const jersey = player?.jersey;
  const position = player?.position;
  const salary = player?.salary;
  const birthplace = player?.city;
  const height = player?.height;
  const weight = player?.weight;
  const teamid = player?.tid;
  const twitter = player?.twitter;
  const college = player?.college;

  console.log(player);

  return loaded && loadedTeam && loadedArena ? (
    <Container>
      <Row>
        <Col>
          <h1>{name}</h1>
        </Col>
      </Row>
      <Row>
        <Col>
          <Image src={picture} fluid width="150"></Image>
          <br></br>
          <br></br>
          <h4>{team} #{jersey}</h4>
          <span style={{fontWeight:'bold'}}>Position: </span>{position} <br></br>
          <br></br>
          <span style={{fontWeight:'bold'}}>Salary: </span>{salary ? `$${salary.toLocaleString()}` : "n/a"} <br></br>
          <span style={{fontWeight:'bold'}}>Birthplace:</span> {birthplace} <br></br>
          <span style={{fontWeight:'bold'}}>College:</span> {college} <br></br>
          <br></br>
          <span style={{fontWeight:'bold'}}>Height:</span> {inchesToFeetAndInches(height)} <br></br>
          <span style={{fontWeight:'bold'}}>Weight:</span> {weight} lbs <br></br>
        </Col>
        <Col>
          <Accordion defaultActiveKey={["1", "2"]} style={{width: "400px"}}>
            <Accordion.Item eventKey="1">
              <Accordion.Header> Related Team </Accordion.Header>
              <Accordion.Body style={{textAlign: 'left' }}>
              <Link to={`/team/${relatedTeam.tid}`}> <Image src={relatedTeam.logo} width={100} />  {'\t'}{relatedTeam.city} {relatedTeam.teamname}</Link>
              </Accordion.Body>
            </Accordion.Item>
            <Accordion.Item eventKey="2">
              <Accordion.Header> Related Arena </Accordion.Header>
              <Accordion.Body style={{textAlign: 'left' }}>
              <Link to={`/arena/${relatedArena.aid}`}><Image src={relatedArena.image}  width={175}/> {relatedArena.arenaname} </Link>
                
              </Accordion.Body>
            </Accordion.Item>
          </Accordion>
        </Col>
      </Row>
      <Row>
        <Col>
        <h5>Twitter</h5>
        {twitter ? <Timeline dataSource={{ url: twitter }} options={{ width: '800' , height: '500'}}></Timeline> : 
        'Could not find this player\'s Twitter.'}
        </Col>
      </Row>
    </Container>
  ) : (
    <Spinner animation="grow" />
  );
};

export default Player;
