import { Container, Row, Col } from 'react-bootstrap';
import { useState, useEffect } from 'react';
import axios from 'axios';
import ArenaCard from '../components/ArenaCard';
import TeamCard from '../components/TeamCard';
import PlayerCard from '../components/PlayerCard';
import { useParams } from 'react-router-dom';

const Search = () => {
    const client = axios.create({ baseURL: import.meta.env.VITE_API_URL });
    const searchQuery = useParams().query;
    const [loaded, setLoaded] = useState(false);
    const [empty, setEmpty] = useState(true);
    const [teams, setTeams] = useState([]);
    const [players, setPlayers] = useState([]);
    const [arenas, setArenas] = useState([]);

    const fetch = async () => {
        console.log("sq")
        console.log(searchQuery);
        if (searchQuery == "") {
            setLoaded(true);
            setEmpty(true);
            return;
        }
        setEmpty(false);
        let queryPlayers = "v1/json/players?offset=1&sort=first&search=" + searchQuery;
        console.log(queryPlayers);
        await client
            .get(queryPlayers, {
                headers: {
                    'Access-Control-Allow-Origin': '*',
                },
            })
            .then((response) => {
                setPlayers(response.data);
                console.log(response.data);
            });

        let queryTeams = "v1/json/teams?offset=1&sort=city&search=" + searchQuery;
        console.log(queryTeams);
        await client
            .get(queryTeams, {
                headers: {
                    'Access-Control-Allow-Origin': '*',
                },
            })
            .then((response) => {
                setTeams(response.data);
            });

        let queryArenas = "v1/json/arenas?offset=1&sort=arenaname&search=" + searchQuery;
        await client
            .get(queryArenas, {
                headers: {
                    'Access-Control-Allow-Origin': '*',
                },
            })
            .then((response) => {
                setArenas(response.data);
                setLoaded(true);
            });
    }

    useEffect(() => {
        if (!loaded) {
            fetch();
        }
    });

    return (
        <Container>
            <Row>
                <h3> Search results for &quot;Players&quot; </h3>
                {loaded && !empty? (
                    players['data'].map((player) => {
                        return (
                            // Send team data to the team card object
                            <Col key={player.id} className="d-flex align-self-stretch">
                                <PlayerCard player={player} highlight={searchQuery} />
                            </Col>
                        );
                    })
                ) : (
                    <br></br>
                )}
            </Row>
            <Row>
                <h3> Search results for &quot;Teams&quot; </h3>
                {loaded && !empty? (
                    teams['data'].map((team) => {
                        return (
                            // Send team data to the team card object
                            <Col key={team.id} className="d-flex align-self-stretch">
                                <TeamCard team={team} highlight={searchQuery} />
                            </Col>
                        );
                    })
                ) : (
                    <br></br>
                )}
            </Row>
            <Row>
                <h3> Search results for &quot;Arenas&quot; </h3>
                {loaded && !empty ? (
                    arenas['data'].map((arena) => {
                        return (
                            // Send arena data to the arena card object
                            <Col key={arena.id} className="d-flex align-self-stretch">
                                <ArenaCard arena={arena} highlight={searchQuery} />
                            </Col>
                        );
                    })
                ) : (
                    <br></br>
                )}
            </Row>
        </Container>
    )
}

export default Search;