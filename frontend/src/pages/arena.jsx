import { useParams, Link } from 'react-router-dom';
import { Timeline } from 'react-twitter-widgets';
import axios from 'axios';
import { useState, useEffect } from 'react';
import Spinner from 'react-bootstrap/Spinner';
import Container from 'react-bootstrap/Container';
import Image from 'react-bootstrap/Image';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { Accordion } from 'react-bootstrap';

const client = axios.create({ baseURL: import.meta.env.VITE_API_URL });

const Arenas = () => {
  const [arena, setArena] = useState([]);
  const [loaded, setLoaded] = useState(false);
  const { id } = useParams();
  
  const [players, setPlayers] = useState([]);
  const [loadedPlayers, setLoadedPlayers] = useState(false);

  const [team, setTeam] = useState([]);
  const [loadedTeam, setLoadedTeam] = useState(false);

  useEffect(() => {
    const fetchArena = async () => {
      await client
        .get('v1/json/arenas/' + id, {
          headers: {
            'Access-Control-Allow-Origin': '*',
          },
        })
        .then((response) => {
          setArena(response.data.data[0]);
        });
      setLoaded(true);
    };
    if (!loaded) {
      fetchArena();
    }
  });

  // fetch the related team 
  useEffect(() => {
    const fetchTeam = async () => {
      await client
        .get('v1/json/teams/' + arena.tid, {
          headers: {
            'Access-Control-Allow-Origin': '*',
          },
        })
        .then((response) => {
          setTeam(response.data.data[0]);
        });
      setLoadedTeam(true);
    };
    if (loaded && !loadedTeam && arena.tid != null) {
      fetchTeam();
    }
  });

  /** Get related players */
  useEffect(() => {
    const fetchPlayers = async () => {
      await client
        .get('v1/json/players?teamshort=' + team.abbreviation + "&sort=last", {
          headers: {
            'Access-Control-Allow-Origin': '*',
          },
        })
        .then((response) => {
          setPlayers(response.data.data)
        });
      setLoadedPlayers(true);
    };
    if (loaded && loadedTeam && !loadedPlayers) {
      fetchPlayers();
    }
  });


  console.log(arena);

  //const stadiumID = arena?.StadiumID;
  const active = arena?.active;
  const name = arena?.arenaname;
  const address = arena?.address;
  const city = arena?.city;
  const state = arena?.state;
  const zip = arena?.zip;
  const country = arena?.country;
  const fullAddr = country == 'USA' ? address + ' ' + city + ', ' + state + ', ' + country + ', ' + zip :
    address + ' ' + city + ', ' + country + ', ' + zip;
  const capacity = arena?.capacity;
  const image = arena?.image ? arena?.image : 'https://thumbs.dreamstime.com/b/basketball-court-arena-stadium-cartoon-empty-hall-field-to-play-basketball-team-game-basketball-court-arena-stadium-vector-217301199.jpg';
  //const geoLat = arena?.GeoLat;
  //const geoLong = arena?.GeoLong;

  function getPlayerDropdowns() {
    let result = []
    players.forEach((player) => {
      result.push(
        <Accordion.Body style={{textAlign: 'left' }}>
          <Link to = {'/player/' + player.id}>
            <Image src={player.photo} />
            {player.first} {player.last}
          </Link>
        </Accordion.Body>
      )
    })
    return result;
  }


  return loaded ? (
    <Container>
      <Row>
        <Col>
          <h1> {name} </h1>
        </Col>
      </Row>
      <Row>
        <Image src={image} fluid width="150"></Image>
      </Row>
      <Row>
        <Col>
          <span style={{ fontWeight: 'bold' }}>Capacity:</span> {capacity ? capacity.toLocaleString() : 'n/a'} <br></br>
          <span style={{ fontWeight: 'bold' }}>Active: </span>{active ? 'Yes' : 'No'} <br></br>
          <br></br>
        </Col>
      </Row>
      <Row>
        <Col>
          <h5>Address</h5>
          {fullAddr} <br></br>
          <br></br>
          <iframe
            width="100%"
            height="300"
            referrerPolicy="no-referrer-when-downgrade"
            src={"https://www.google.com/maps/embed/v1/place?key=AIzaSyDtjwvKNTZ6iILF96SnYjPlrJS-bWpt6vA&q=" + fullAddr}
            allowFullScreen>
          </iframe>
          <br></br>
          <br></br>
        </Col>
      </Row>
      <Row>
        {loadedTeam ? (
        <Accordion defaultActiveKey={["1", "2"]}>
          <Accordion.Item eventKey = "2">
            <Accordion.Header>Related Team </Accordion.Header>
            <Accordion.Body style={{textAlign: 'left' }}>
              <Link to={`/team/${team.tid}`}><Image src={team.logo} width={100} /> {'\t'}{team.city} {team.teamname} </Link>
            </Accordion.Body>
          </Accordion.Item>
          <Accordion.Item eventKey = "1" >
            <Accordion.Header>Related Players </Accordion.Header>
            {getPlayerDropdowns()}
          </Accordion.Item>
        </Accordion>) 
        : ("") }
      </Row>
    </Container>
  ) : (
    <Spinner animation="grow"></Spinner>
  );
}


export default Arenas;
