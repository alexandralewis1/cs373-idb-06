import '../App.css';
import 'bootswatch/dist/lux/bootstrap.min.css';
import { Container, Row, Col, Carousel } from 'react-bootstrap'
import { HomeCard } from '../components/HomeCard.jsx';
import arenaPic from '../assets/home-pictures/aa_arena.jpg'
import teamPic from '../assets/home-pictures/miamiheat.jpeg'
import playerPic from '../assets/home-pictures/lebronjames.jpg'
import nba from '../assets/home-pictures/nba.jpeg'
import nba2 from '../assets/home-pictures/nba2.jpg'
import nba3 from '../assets/home-pictures/nba3.jpg'


function Landing() {
  //const [count, setCount] = useState(0);

  return (

    <div>
    <Carousel>
        <Carousel.Item>
            <img className="d-block w-100 img-carousel" src={nba2} />
            <Carousel.Caption>
            <h3 style={{ color: 'white' }}>NBA Database</h3>
            <p>The premier location to find information on your favorite NBA players, teams and arenas from the 2022-2023 season.</p>
            </Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item>
            <img className="d-block w-100 img-carousel" src={nba} />
        </Carousel.Item>
        <Carousel.Item>
            <img className="d-block w-100 img-carousel" src={nba3} />
        </Carousel.Item>
    </Carousel>

    <Container className="my-5">
        <Row>
            <Col>
            <HomeCard 
                title="Players"
                subtitle=""
                text="Find your favorite players!"
                imgURL={playerPic}
                linkURL="/players"
            />
            </Col>
            
            <Col>
            <HomeCard 
                title="Teams"
                subtitle=""
                text="Learn more about your favorite NBA teams!"
                imgURL={teamPic}
                linkURL="/teams"
            />
            </Col>
            <Col>
                <HomeCard 
                    title="Arenas"
                    subtitle=""
                    text="Find where your favorite teams play!"
                    imgURL={arenaPic}
                    linkURL="/arenas"
                />
            </Col>
        </Row>
        
    </Container>
</div>
  );
}

export default Landing;
