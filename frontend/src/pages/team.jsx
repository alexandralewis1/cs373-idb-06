//import React from 'react';
import { useParams, Link } from 'react-router-dom';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import {Image, Accordion, Card, Carousel} from 'react-bootstrap';
import { GetColorName } from 'hex-color-to-color-name';
import axios from 'axios';
import { useState, useEffect } from 'react';
import Spinner from 'react-bootstrap/Spinner';
import { Timeline } from 'react-twitter-widgets';

const client = axios.create({ baseURL: import.meta.env.VITE_API_URL });

/**
 * This class contains the extended information for any given team
 */
const Team = () => {
  const [team, setTeam] = useState([]);
  const [loaded, setLoaded] = useState(false);

  /* Get related players and arena */
  const [players, setPlayers] = useState([]);
  const[loadedPlayers, setLoadedPlayers] = useState(false);
  const [arena, setArena] = useState([]);
  const[loadedArena, setLoadedArena] = useState(false);
  
  const { id } = useParams();

  // get the team 
  useEffect(() => {
    const fetchTeam = async () => {
      await client
        .get('v1/json/teams/' + id, {
          headers: {
            'Access-Control-Allow-Origin': '*',
          },
        })
        .then((response) => {
          setTeam(response.data.data[0]);
        });
      setLoaded(true);
    };
    if (!loaded) {
      fetchTeam();
    }
  });

  /** Get related players */
  useEffect(() => {
    const fetchPlayers = async () => {
      await client
        .get('v1/json/players?teamshort=' + team.abbreviation + "&sort=last", {
          headers: {
            'Access-Control-Allow-Origin': '*',
          },
        })
        .then((response) => {
          setPlayers(response.data.data)
        });
        setLoadedPlayers(true);
    };
    if (loaded && !loadedPlayers) {
      fetchPlayers();
    }
  });

  /** Get related arenas */
  useEffect(() => {
    const fetchArena = async () => {
      await client
        .get('v1/json/arenas/' + team.aid, {
          headers: {
            'Access-Control-Allow-Origin': '*',
          },
        })
        .then((response) => {
          setArena(response.data.data[0])
          console.log(response.data.data[0])
        });
        setLoadedArena(true);
    };
    if (loaded && !loadedArena) {
      fetchArena();
    }
  });


  // const key = team?.Key;
  const abbrev = team?.abbreviation;
  const active = team?.active;
  const city = team?.city;
  const teamName = team?.city + ' ' + team?.teamname;
  const picture = team?.logo;
  const conference = team?.conference;
  const division = team?.division;
  const primaryColor = team?.primarycolor
  const secondaryColor = team?.secondarycolor;
  const tertiaryColor = team?.tertiarycolor ? team?.tertiarycolor : "000000";
  const aid = team?.aid;
  const twitter = team?.twitter;
  const founded = team?.founded;
  const championships = team?.championships;

  
  function getPlayerDropdowns() {
    let result = []
    players.forEach((player) => {
      result.push(
        <Accordion.Body style={{textAlign: 'left' }}>
          <Link to = {'/player/' + player.id}>
            <Image src={player.photo} />
            {player.first} {player.last}
          </Link>
        </Accordion.Body>
      )
    })
    return result;
  }
  

  {
    return loaded && loadedPlayers && loadedArena ? (
      <Container>
        <Row>
          <Col>
            <h1>{teamName}</h1>
          </Col>
        </Row>
        <Row>
          <Col>
          </Col>
        </Row>
        <Row>
          <Col>
          <Image src={picture} fluid width="150"></Image>
            <br></br>
            <br></br>
            <span style={{fontWeight:'bold'}}>Conference:</span>  {conference}<br></br>
            <span style={{fontWeight:'bold'}}>Division:</span> {division} <br></br>
            <span style={{fontWeight:'bold'}}>Active: </span>{active ? 'Yes' : 'No'} <br></br>
            <span style={{fontWeight:'bold'}}>Year founded: </span>{founded} <br></br>
            <span style={{fontWeight:'bold'}}>Championships won: </span>{championships} <br></br>
            <br></br>
            <h5>Team Colors:</h5>
            <span style={{ color: '#'+primaryColor, fontWeight:'bold'}}>{GetColorName(primaryColor)}</span> <br></br>
              <span style={{ color: '#'+secondaryColor, fontWeight:'bold' }}>{GetColorName(secondaryColor)}</span> <br></br>
              <span style={{ color: '#'+tertiaryColor, fontWeight:'bold' }}>{GetColorName(tertiaryColor)}</span> <br></br>
            <br></br>
            <br></br>
          </Col>
          <Col>
            <Accordion defaultActiveKey={["1", "2"]} style={{width: "400px"}}>
              <Accordion.Item eventKey = "1" style={{maxHeight: "300px", overflow: "auto" }}>
                <Accordion.Header> Related Players </Accordion.Header>
                {getPlayerDropdowns()}
              </Accordion.Item>
              <Accordion.Item eventKey= "2">
                <Accordion.Header> Related Arena </Accordion.Header>
                <Accordion.Body style={{textAlign: 'left' }}>
                  <Link to = {'/arena/' + arena.aid}>
                    <Image src={arena.image} width="100"/>
                    {"\t"}{arena.arenaname}
                  </Link>
                </Accordion.Body>
              </Accordion.Item>
            </Accordion>
          </Col>
        </Row>
        <Row>
            <Col>
              <h5>Twitter</h5>
              { <Timeline dataSource={{ url: twitter }} options={{ width: '800', height: '500' }}></Timeline> }
              <br></br>
            </Col>
        </Row>
      </Container>
    ) : (
      <Spinner animation="grow" />
    );
  }
};

export default Team;
