import 'bootswatch/dist/lux/bootstrap.min.css';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import TeamCard from '../components/TeamCard';
import axios from 'axios';
import { useState, useEffect } from 'react';
import Pagination from 'react-bootstrap/Pagination';
import Spinner from 'react-bootstrap/Spinner';
import { Container, Form, Button } from 'react-bootstrap';
import Sidebar from '../components/Sidebar';

const client = axios.create({ baseURL: import.meta.env.VITE_API_URL });
console.log(import.meta.env.VITE_API_URL)
/**
 * Displays all the team cards
 */
const Teams = () => {
  /**
   * Data for showing the cards
   */
  // Initialize empty array of teams
  const [currentTeams, setCurrentTeams] = useState([]);
  // Number of teams return 
  const [count, setCount] = useState(0);
  // We have not yet loaded the data
  const [loaded, setLoaded] = useState(false);
  // What page we are on
  const [currentPage, setCurrentPage] = useState(2);
  // All players - loaded for filtering purposes 
  const [allTeams, setAllTeams] = useState([]);
  const [allLoad, setAllLoad] = useState(false);
  // items per page 
  const ITEMS_PER_PAGE = 20;
  // items returned 
  const [itemsReturned, setItemsReturned] = useState(ITEMS_PER_PAGE);

  /**
   * Parameters for filteirng 
   */
  const [conference, setConference] = useState("All");
  const [division, setDivision] = useState("All")
  const [active, setActive] = useState("All")
  // Create the map for the filter bar 
  let categories = new Map([
    ['conference', 'Conference'],
    ['division', 'Division'],
    ['active', 'Active'],
  ]);

  /**
  * parameters for sorting 
  */
  const [selectedOption, setSelectedOption] = useState("city");

  /**
 * parameters for querying 
 * @param {} pageNumber 
 */
  const [searchBarInput, setSearchbarInput] = useState("")
  const [searchQuery, setSearchQuery] = useState("")

  // function that changes the page 
  function pageChange(pageNumber) {
    setCurrentPage(pageNumber);
    setLoaded(false);
  }

  /**
   * Handles a filter call
   * @param {*} filterCategory: string indicating the category to be filtered 
   * @param {*} selection: either a string with the name or an array with [min, max]
   */
  const handleFilterChange = (filterCategory,
    selection = null) => {
    if (filterCategory == "Conference") {
      setConference(selection);
    }
    if (filterCategory == "Division") {
      setDivision(selection)
    }
    if (filterCategory == "Active") {
      setActive(selection)
    }
    setLoaded(false);
  }

  /**
  * Handles the sort
  */

  const handleSort = async (event) => {
    setSelectedOption(event.target.value);
    setLoaded(false);
  };

  /**
 * Handles a query 
*/
  const handleSearch = (e) => {
    console.log("Search")
    e.preventDefault()

    setSearchQuery(searchBarInput.replace(" ", "+"))
    console.log(searchQuery)

    setLoaded(false)
  }


  // On load 
  useEffect(() => {
    // Read teams from database
    const fetchTeams = async () => {
      let query = "v1/json/teams?offset=" + (currentPage - 1) + "&limit=" + ITEMS_PER_PAGE + "&sort=" + selectedOption
      //KT
      if (conference != "All") {
        query += "&conference=" + conference
      }
      if (division != "All") {
        query += "&division=" + division
      }
      if (searchQuery != "") {
        query += "&search=" + searchQuery
      }
      console.log(query);
      /**
       * TODO - implement search and filter calls to the API
       */
      await client
        .get(query, {
          headers: {
            'Access-Control-Allow-Origin': '*',
          },
        })
        .then((response) => {
          console.log(response.data)
          setItemsReturned(response.data.data.length);
          setCount(response.data.total);
          setCurrentTeams(response.data);
        });
      setLoaded(true);
    };

    const fetchAll = async () => {
      await client
        .get('v1/json/teams', {
          headers: {
            'Access-Control-Allow-Origin': '*',
          },
        })
        .then((response) => {
          setAllTeams(response.data);
          //console.log("all ", allTeams) 
          setAllLoad(true);
        });
    };
    // Load the teams if no done yet
    if (!loaded) {
      fetchTeams();
    }
    if (!allLoad) {
      fetchAll();
    }
  });

  // Create the pagination bar - since doing paginated queries dont know how many pages to make (hard code math?)
  const numPages = (count % ITEMS_PER_PAGE) == 0 ? (count / ITEMS_PER_PAGE) : (Math.floor(count / ITEMS_PER_PAGE) + 1);
  const items = [];
  for (let i = 1; i <= numPages; i++) {
    items.push(
      <Pagination.Item key={i} onClick={() => pageChange(i + 1)}>
        {i}
      </Pagination.Item>,
    );
  }

  return (
    // Returns a grid of cards
    <Container>
      <Row>
        <h1>Teams</h1>
        <Form onSubmit={handleSearch}>
          <Form.Control 
          type="search" 
          placeholder="Search Teams"
          value = {searchBarInput}
          onChange = {(e) => setSearchbarInput(e.target.value)}></Form.Control> <br></br>
          <br></br>
          <Button type="submit"> Submit </Button>
        </Form>
      </Row>
      <Row>
        <Row>
          <Col></Col>
          <Col></Col>
          <Col>
            <Form.Select
            onChange = {handleSort}
            value = {selectedOption}>
              <option value = "city">Sort By City: A-Z</option>
              <option value = "teamname"> Sort By Name: A-Z </option>
              <option value = "conference">Sort By Conference: A-Z</option>
              <option value = "division">Sort By Division: A-Z</option>
            </Form.Select> <br />
          </Col>
        </Row>
        <Col xs={2}>
          {allLoad ? (<Sidebar data={allTeams} categories={categories} handleFilterChange={handleFilterChange} />) : <Spinner animation="grow" />}
        </Col>
        <Col xs={10}>
          <Row xl={4}
            lg={3}
            md={2}
            sm={1}
            xs={1}
            className="d-flex g-4 p-4 justify-content-center">
            {loaded ? (
              currentTeams['data'].map((team) => {
                return (
                  // Send team data to the team card object
                  <Col key={team.id} className="d-flex align-self-stretch" style={{minWidth:"225px"}}>
                    <TeamCard team={team} highlight={searchQuery} />
                  </Col>
                );
              })
            ) : (
              <Spinner animation="grow" />
            )}
          </Row>
        </Col>
      </Row>
      <Row>
        <Pagination className="justify-content-center">{items}</Pagination>
        <h9>Page {currentPage - 1} / {numPages}{' '} <br /> </h9>
        <h9>Showing {itemsReturned} of {count} instances </h9>
      </Row>
    </Container>
  );
};

export default Teams;
