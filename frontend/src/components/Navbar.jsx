import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import { Container, Form, FormControl, Button, InputGroup } from 'react-bootstrap';
import { useState } from 'react';

const WebsiteNavbar = () => {
  const [searchBarInput, setSearchBarInput] = useState("");

  const handleSearch = (e) => {
    e.preventDefault();
    const query = searchBarInput.replace(" ", "+")
    window.location.href = '/search/' + query;
  }

  return (
    <Navbar bg="primary" variant="dark" expand="lg" fixed="top">
      <Container>
        <Navbar.Brand href="/">NBADb</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="me-auto">
            <Nav.Link href="/">Home</Nav.Link>
            <Nav.Link href="/about">About</Nav.Link>
            <Nav.Link href="/players">Players</Nav.Link>
            <Nav.Link href="/teams">Teams</Nav.Link>
            <Nav.Link href="/arenas">Arenas</Nav.Link>
            <Nav.Link href="/visualizations">Visualizations</Nav.Link>
            <Nav.Link href="/providervisualizations">Provider Visualizations</Nav.Link>
          </Nav>
          <Form inline onSubmit={handleSearch}>
            <InputGroup>
              <FormControl type="search" 
              placeholder="Search NBADB" 
              value={searchBarInput}
              onChange={(e) => setSearchBarInput(e.target.value)} />
              <Button variant="outline-success" type="submit">Go</Button>
            </InputGroup>
          </Form>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}

export default WebsiteNavbar;
