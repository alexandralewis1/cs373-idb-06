
import { Card, Button } from 'react-bootstrap'

import { LinkContainer } from 'react-router-bootstrap'
import PropTypes from 'prop-types';

export function HomeCard(props) {
    return (
        <Card style={{height: "500px"}}>
            <Card.Img
                variant='top'
                alt="Card image cap"
                src={props.imgURL}
                width="100%"
            />
            <Card.Body>
                <Card.Title>{props.title}</Card.Title>
                <Card.Text>{props.text}</Card.Text>
                
                <LinkContainer to={props.linkURL}>
                    <Button id="btn">More Info</Button>
                </LinkContainer>
            </Card.Body>
        </Card>
    );
}

HomeCard.propTypes = {
    imgURL: PropTypes.string,
    title:  PropTypes.string,
    linkURL:  PropTypes.string,
    text:  PropTypes.string
}

export default HomeCard