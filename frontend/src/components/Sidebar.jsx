import React from "react";
import { Accordion, Form, Button } from 'react-bootstrap'
import { useState, useEffect } from 'react';
import PropTypes from 'prop-types';

const Sidebar = (props) => {
    /** All player data, all arena data, or all team data */
    const data = props.data;
    //console.log(data);
    /** Categories that will be in the side bar 
     * Map such that key=apiNickname, value=menuName 
     * Example: 'teamshort' : 'Team'
     */
    const categories = props.categories;
    /**
     * Categories that will have a numerical filter typed in
     * Rather than a dropdown menu 
     */
    let specialCategories = new Set(['Salary', 'Capacity'])
    /**
     * Contains the data stored in the sidebar 
     */
    let sidebarContents = createFilters(createFilterDict())


    /**
     * Given a dictionary that matches each filterable attribute to its possible values,
     * Build the overall accordion dropdown object that will store this menu 
     * @param {*} filterDict 
     * @returns 
     */
    function createFilters(filterDict) {
        let filters = []
        filterDict.forEach((dropdownButtons, filterableAttributeName) => {
            filters.push(
                <Accordion.Item eventKey={filterableAttributeName}>
                    <Accordion.Header> {filterableAttributeName} </Accordion.Header>
                    <Accordion.Body style={{maxHeight: "300px", overflow: "auto" }}> {dropdownButtons}</Accordion.Body>
                   
                </Accordion.Item>
            )
        })
        return filters
    }

    /**
   * The sidebar filter data 
   * Map where key=categoryName, value= list of filterables 
   * Example: key=Team, values=[DAL, WAS, etc.]
   */
    function createFilterDict() {
        const [minVal, setMinVal] = useState(Number.MAX_SAFE_INTEGER)
        const [maxVal, setMaxVal] = useState(Number.MIN_SAFE_INTEGER)
        let result = new Map()
        // Iterate through each category that will be a dropdown in the sidebar
        // Example: team, position, country, ...
        categories.forEach((filterCategory, apiNickname) => {
            const onSubmit = () => {
                props.handleFilterChange(filterCategory, [minVal, maxVal])
            }
            // Handle the case where you manually enter a filter value 
            if (specialCategories.has(filterCategory)) {
                let dropdownItems = []
                // Push a box to enter in the min value 
                dropdownItems.push(
                    <Accordion.Body> Min:
                        <Form onChange={e => setMinVal(e.target.value)}>
                            <Form.Control type="number" />
                        </Form>
                    </Accordion.Body>
                )
                // Push a box to enter in the max value 
                dropdownItems.push(
                    <Accordion.Body> Max:
                        <Form onChange={e => setMaxVal(e.target.value)}>
                            <Form.Control type="number" />
                        </Form>
                        <Button onClick={onSubmit}>
                            Enter
                        </Button>
                        <br /> <br />
                        <Button onClick={() => props.handleFilterChange(filterCategory, "All")}>
                            Clear
                        </Button>
                    </Accordion.Body>
                )
                // Add the k-v pair: category (e.g. team) -> items (e.g. dal, was, etc.)
                result.set(filterCategory, dropdownItems)
            } else {
                // get all the unique items for the drop down
                // from the data returned from the api call 
                let dropdownItemsSet = new Set()
                data.data.forEach((dp) => {
                    if (apiNickname == "name" && "first" in dp) {
                        dropdownItemsSet.add(dp["first"] + dp["last"])
                    }

                    else {
                        dropdownItemsSet.add(String(dp[apiNickname]))
                    }

                })

                // sort
                let sortedSet = Array.from(dropdownItemsSet).sort()

                // add each individual link component 
                let dropdownItems = []
                sortedSet.forEach((selection) => {
                    dropdownItems.push(
                        // Name of each team, city, etc. 
                            <Button variant="link" onClick={() => props.handleFilterChange(filterCategory, selection)}>
                                {selection}
                            </Button>
                        
                    )
                })

                // add the clear button 
                dropdownItems.push(
                    <Accordion.Body>
                        <Button onClick={() => props.handleFilterChange(filterCategory, "All")}>
                            Clear
                        </Button>
                    </Accordion.Body>
                )

                // map the category to the link/button items in the  
                result.set(filterCategory, dropdownItems)
            }
        })
        return result
    }

    return (
        <Accordion className="flex-column">
            {sidebarContents}
        </Accordion >
    )
}

Sidebar.propTypes = {
    data: PropTypes.array,
    categories: PropTypes.array,
    handleFilterChange: PropTypes.func
}

export default Sidebar;