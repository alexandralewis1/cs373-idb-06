import Card from 'react-bootstrap/Card';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import Highlighter from "react-highlight-words";

const TeamCard = (props) => {
  const picture = props.team.logo;
  const name = props.team.city + " " + props.team.teamname;
  const abbrev = props.team.abbreviation;
  const city = props.team.city;
  const conference = props.team.conference;
  const division = props.team.division;
  const teamid = props.team.tid;
  const active = props.team.active;

  const query = props.highlight.trim();

  TeamCard.propTypes = {
    team: PropTypes.shape({
      logo: PropTypes.string.isRequired,
      teamname: PropTypes.string.isRequired,
      abbreviation: PropTypes.string.isRequired,
      city: PropTypes.string.isRequired,
      conference: PropTypes.string.isRequired,
      division: PropTypes.string.isRequired,
      tid: PropTypes.number.isRequired,
      active: PropTypes.bool,
    }),
    highlight: PropTypes.string,
  };

  function highlight(text) {
    return <Highlighter
              searchWords={query.split("+")}
              autoEscape={true}
              textToHighlight={text}
              />
  }

  return (
    <Card border="dark">
      <Card.Body>
        <Card.Title> {highlight(name)} </Card.Title>
        <Card.Img src={picture}></Card.Img>
        <Card.Text />
        <Card.Subtitle> {highlight(abbrev)} </Card.Subtitle>
        <Card.Text>City: {highlight(city)}</Card.Text>
        <Card.Text>Conference: {highlight(conference)}</Card.Text>
        <Card.Text>Division: {highlight(division)}</Card.Text>
        <Card.Text>{active ? "Active" : "Inactive"}</Card.Text>
      
      </Card.Body>
      <Card.Footer className="text-muted">
        <Link to={`/team/${teamid}`}>More info</Link>
      </Card.Footer>
    </Card>
  );
};

export default TeamCard;