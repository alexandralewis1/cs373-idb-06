import Card from 'react-bootstrap/Card';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import Highlighter from "react-highlight-words";

function getLocation(city, state, country) {
  if (state == null || state == "") {
    return city + ", " + country;
  }
  return city + ", " + state + ", " + country;
}

const ArenaCard = (props) => {

  const name = props.arena.arenaname;
  const capacity = props.arena.capacity;
  const location = getLocation(props.arena.city, props.arena.state, props.arena.country);
  const activeStatus = props.arena.active;
  const arenaId = props.arena.aid;
  const image = props.arena.image ? props.arena.image : "https://thumbs.dreamstime.com/b/basketball-court-arena-stadium-cartoon-empty-hall-field-to-play-basketball-team-game-basketball-court-arena-stadium-vector-217301199.jpg";
  const query = props.highlight.trim();

  ArenaCard.propTypes = {
    arena: PropTypes.shape({
      arenaname: PropTypes.string.isRequired,
      capacity: PropTypes.number.isRequired,
      city: PropTypes.string.isRequired,
      state: PropTypes.string.isRequired,
      country: PropTypes.string.isRequired,
      active: PropTypes.bool.isRequired,
      aid: PropTypes.string.isRequired,
      image: PropTypes.string,
    }),
    highlight: PropTypes.string,
  };

  function highlight(text) {
    return <Highlighter
              searchWords={query.split("+")}
              autoEscape={true}
              textToHighlight={text}
              />
  }

  return (
    <Card border="dark">
      <Card.Body>
        <Card.Title> {highlight(name)} </Card.Title>
        <Card.Img src={image}></Card.Img>
        <Card.Text />
        <Card.Subtitle> Capacity: {capacity != null ? capacity.toLocaleString() : "N/A"} </Card.Subtitle>
        <Card.Text>Location: {highlight(location)}</Card.Text>
        <Card.Text>{activeStatus ? "Active" : "Inactive"}</Card.Text>
        <Card.Text>
        </Card.Text>
      </Card.Body>
      <Card.Footer className="text-muted">
        <Link to={`/arena/${arenaId}`}>More info</Link>
      </Card.Footer>
    </Card>
  );
};

export default ArenaCard;
