import Card from 'react-bootstrap/Card';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import Highlighter from "react-highlight-words";

function calculateAge(dateString) {
  // get a date object
  var dob = new Date(dateString);
  var today = new Date();
  
  // calculate the age in years
  var age = today.getFullYear() - dob.getFullYear();
  var monthDifference = today.getMonth() - dob.getMonth();
  if (monthDifference < 0 || (monthDifference === 0 && today.getDate() < dob.getDate())) {
    age--;
  }
  
  return age;
}


const PlayerCard = (props) => {
  const picture = props.player.photo;
  const name = props.player.first + ' ' + props.player.last;
  const team = props.player.teamshort;
  const teamPage = `/team/` + props.player.tid;
  const country = props.player.country;
  const salary = props.player.salary;
  const position = props.player.position;
  const playerPage = `/player/` + props.player.id;
  const date = props.player.birthday;

  const query = props.highlight.trim();

  function highlight(text) {
    return <Highlighter
              searchWords={query.split("+")}
              autoEscape={true}
              textToHighlight={text}
              />
  }

  return (
    <Card>
      <Card.Body>
        <Card.Title> {highlight(name)} </Card.Title>
        <Card.Img src={picture} style={{ width: '5rem' }}></Card.Img>
        <Card.Text />
        <Card.Subtitle> {highlight(team)} </Card.Subtitle>
        <Card.Text></Card.Text>
        <Card.Text> Country of Origin: {highlight(country)} </Card.Text>
        <Card.Text>Salary: {salary ? "$" + salary.toLocaleString() : "N/A"} </Card.Text>
        <Card.Text> Position: {highlight(position)} </Card.Text>
        <Card.Text> Age: {calculateAge(date)} </Card.Text>
      </Card.Body>
      <Card.Footer>
        <Link to={playerPage}>More info</Link>
      </Card.Footer>
    </Card>
  );
};

PlayerCard.propTypes = {
  player: PropTypes.shape({
    photo: PropTypes.string.isRequired,
    first: PropTypes.string.isRequired,
    last: PropTypes.string.isRequired,
    teamshort: PropTypes.string.isRequired,
    tid: PropTypes.number.isRequired,
    country: PropTypes.string.isRequired,
    salary: PropTypes.number,
    position: PropTypes.string.isRequired,
    id: PropTypes.number.isRequired,
    birthday: PropTypes.string.isRequired,
  }),
  highlight: PropTypes.string,
};


export default PlayerCard;
