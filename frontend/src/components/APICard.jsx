import Card from 'react-bootstrap/Card';
import PropTypes from 'prop-types';
import Container from 'react-bootstrap/Container';
import { Link } from 'react-router-dom';

const APICard = (props) => {
  return (
    <Container style={{ width: '100%', marginTop: 20 }}>
      <Card style={{ width: '15rem' }}>
        <Card.Img variant="top" src={props.info.image} />
        <Card.Body>
          <Card.Title>{props.info.name}</Card.Title>
          <Card.Text>{props.info.description}</Card.Text>
          <Link to={props.info.link}>More Details</Link>
        </Card.Body>
      </Card>
    </Container>
  );
};

APICard.propTypes = {
  info: PropTypes.shape({
    name: PropTypes.string.isRequired,
    image: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    link: PropTypes.string.isRequired,
  }),
};

export default APICard;
