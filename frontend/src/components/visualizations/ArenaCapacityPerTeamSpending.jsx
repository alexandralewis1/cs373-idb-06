import React, { useRef, useEffect, useState } from "react";
import * as d3 from "d3";
import axios from 'axios';

const client = axios.create({ baseURL: import.meta.env.VITE_API_URL });

const CapacityPerSpending = () => {
  const ref = useRef(null);
  //Number of teams return 
  const [data, setData] = useState([]);  
  const [loaded, setLoaded] = useState(false);
  useEffect(() => {
    var arenadata;
    const fetchData = async () => {
      await client
        .get('v1/json/arenas', {
          headers: {
            'Access-Control-Allow-Origin': '*',
          },
        })
        .then(async (res) => {
          arenadata = res.data.data
          const result = {};
          for(let i = 0; i < arenadata.length; i++) {
            if (arenadata[i].tid != null) {
              result[arenadata[i].tid] = {arenaName: arenadata[i].arenaname, capacity: arenadata[i].capacity}
            }
           
          }
          arenadata = result
          await client
          .get('v1/json/teams', {
            headers: {
              'Access-Control-Allow-Origin': '*',
            },
          })
          .then(async (response) => {
            const teams = response.data.data
            var teamToColor = {}
            for(let i = 0; i < teams.length; i++) {
              teamToColor[teams[i].tid] = {teamName: teams[i].abbreviation, pcolor: '#' + teams[i].primarycolor, scolor: '#' + teams[i].secondarycolor, tcolor: '#' + teams[i].tertiarycolor, qcolor: '#' + teams[i].quaternarycolor,
            }
          }
            await client
            .get('v1/json/players', {
              headers: {
                'Access-Control-Allow-Origin': '*',
              },
            })
            .then((response) => {
              const apiData = response.data.data
    
    
              const result = {};
    
              for (let i = 0; i < apiData.length; i++) {
                var team = apiData[i].teamshort;
                var tid = apiData[i].tid
                if(apiData[i].salary !== null) {
                  if (result[tid]) {
                    result[tid].total = result[tid].total + apiData[i].salary;
                  } else {
                    result[tid] = {teamName: team, total: apiData[i].salary}
                  }
                }
                
              }
              const countsArr = Object.keys(result).map((key) => {
                return Object.assign({teamName: result[key].name, total: result[key].total}, teamToColor[key]);
              });
              
              // // Get top 40 countries by count
              // const top40Data = sortedData.slice(0, 50);
              
  
              const merged = Object.keys(countsArr).reduce((acc, key) => {
                if (Object.prototype.hasOwnProperty.call(arenadata, key)) {
                  acc.push({
                    ...countsArr[key],
                    ...arenadata[key],
                  });
                }
                return acc;
              }, []);
  
              setData(merged);
              setLoaded(true);
            });
          
          });
          
        });
       
    };

    
    if(!loaded) {
      fetchData();
    }
    else {
       // Dimensions
    const margin = { top: 40, right: 70, bottom: 40, left: 70 };
    const width = 960 - margin.left - margin.right;
    const height = 500 - margin.top - margin.bottom;

    // Create the SVG container
    const svg = d3
      .select(ref.current)
      .attr("width", width + margin.left + margin.right)
      .attr("height", height + margin.top + margin.bottom)
      .append("g")
      .attr("transform", `translate(${margin.left},${margin.top})`);

    // Create the x and y scales
    const xScale = d3
      .scaleLinear()
      .domain([d3.min(data, (d) => d.capacity) - 500, d3.max(data, (d) => d.capacity) + 500])
      .range([0, width]);

    const yScale = d3
      .scaleLinear()
      .domain([d3.min(data, (d) => d.total) - 10_000_000, d3.max(data, (d) => d.total) + 10_000_000])
      .range([height, 0]);

    // Create the x and y axes
    const xAxis = d3.axisBottom(xScale).tickFormat(d3.format(".3s"));
    const yAxis = d3.axisLeft(yScale).tickFormat(d3.format(".2s"));
    const colorScales = {};


    data.forEach(d => {
      const primaryColor = d.pcolor;
      const secondaryColor = d.scolor;
      const tertiaryColor = d.tcolor;
      const quadColor = d.qcolor;
      const gradientScale = d3.scaleLinear()
        .domain([0, 1])
        .range([primaryColor, secondaryColor, tertiaryColor, quadColor])
        .interpolate(d3.interpolateRgb);
      colorScales[d.arenaName] = gradientScale;
    });
    
    const colorScale = arenaName => colorScales[arenaName];
    
    const testDomainValue = "Capital One Arena";
    const gradientScale = colorScale(testDomainValue)(0.5);
    
    // const testValue = 0.5;
    // const testColor = gradientScale(testValue);
    
    

    const tooltip = d3.select("body")
      .append("div")
      .attr("class", "tooltip")
      .style("opacity", 0);
    // Add the x and y axes to the SVG container

    svg.append('text') // add a title to the pie chart
      .attr("class", "graph-title")
      .attr('x', width / 2)
      .attr('y', 0)
      .attr('text-anchor', 'middle')
      .style("font-size", "22px") // increase font size to 24 pixels
      .style("text-decoration", "underline") // add underline
      .style("text-decoration", "bold") // add underline
      .style("font-weight", "bold")
      .text('Arena Capacity vs Team Spending');

    svg.append("g")
      .append("text")
      .attr("class", "axis-label")
      .attr("x", width / 2)
      .attr("y", height + 35)
      .style("text-anchor", "middle")
      .text("Arena Capacity");
    svg
      .append("g")
      .attr("transform", `translate(0,${height})`)
      .call(xAxis)
      // .append("text")
      // .attr("class", "label")
      // .attr("x", width /2 )
      // .attr("y", height + 30)
      // .style("text-anchor", "end")
      // .text("Arena Capacity");

    svg
      .append("g")
      .call(yAxis)
    
    svg.append("g")
      .append("text")
      .attr("class", "axis-label")
      .attr("transform", "rotate(-90)")
      .attr("y", 0 - margin.left)
      .attr("x", 0 - (height / 2))
      .attr("dy", "1em")
      .style("text-anchor", "middle")
      .text("Total Spending ($)");
    // Add the data points to the SVG container as circles
    // Create circles and color them using the color scale
    svg.selectAll("circle")
      .data(data)
      .enter()
      .append("circle")
      .attr("cx", d => xScale(d.capacity))
      .attr("cy", d => yScale(d.total))
      .attr("r", 5)
      .attr("fill", d => colorScale(d.arenaName)(0.33))
      .on("click", function(d) {
        d3.select(this).attr("r", 10);
        tooltip.transition()
          .duration(200)
          .style("opacity", .9);
        tooltip.html(`Arena: ${d.srcElement.__data__.arenaName}<br> Team: ${d.srcElement.__data__.teamName}<br>Capacity: ${d.srcElement.__data__.capacity}`)
          .style("left", (d.x + 25) + "px")
          .style("top", (d.y - 30) + "px");
      })
      .on("mouseout", function(d) {
        d3.select(this).attr("r", 5);
        tooltip.transition()
          .duration(500)
          .style("opacity", 0);
    });
    }
   

  }, [data]);

  return <svg ref={ref} width={1400} height={700}></svg>;
};

export default CapacityPerSpending;
