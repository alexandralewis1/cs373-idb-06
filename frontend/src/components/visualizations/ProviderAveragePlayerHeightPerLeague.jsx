
import React, { useEffect, useState } from "react";
import { CartesianGrid, XAxis, YAxis, Bar, ResponsiveContainer, BarChart } from 'recharts';

import axios from 'axios';
const client = axios.create({ baseURL: "https://api.allthesports.info/" });

const ProviderAveragePlayerHeightPerLeague = () => {
  const [data, setData] = useState([]);  
  const [loaded, setLoaded] = useState(false);
  useEffect(() => {
    const fetchData = async () => {
      await client
        .get('leagues').then(async (league) => {
          const apiData = league.data;
          const result = [];
          for (let i = 0; i < apiData.length; i++) {
            let avgHeight = 0;
            let numPlayers = 0;
            await client.get('teams/league/' + apiData[i].id).then(async (team) => {
              for (let j = 0; j < team.data.length; j++) {
                await client.get('players/team/' + team.data[j].id).then(async (players) => {
                  if (!players.data.error) {
                    for (let player of players.data) {
                      avgHeight += player.height;
                      numPlayers++;
                    }
                  }
                })
              }
            })
            avgHeight = avgHeight / numPlayers;
            result.push({category: apiData[i].name, average: avgHeight})
          }
          setData(result);
          setLoaded(true);
        });
    };

    if (!loaded) {
      fetchData();
    }
  }, [data]);
    return (
      <ResponsiveContainer width="100%" height={300}>
        <BarChart
          data={data}
          margin={{
            top: 5,
            right: 30,
            left: 20,
            bottom: 5,
          }}
        >
          <CartesianGrid strokeDasharray="3 3" />
          <XAxis dataKey="category"/>
          <YAxis />
          <Bar dataKey="average" fill="82ca9d" />
        </BarChart>
      </ResponsiveContainer>
    );
}

export default ProviderAveragePlayerHeightPerLeague;