
import React, { useEffect, useState } from "react";
import {  Radar, RadarChart, PolarGrid, PolarAngleAxis, PolarRadiusAxis, ResponsiveContainer } from 'recharts';
import axios from 'axios';
const client = axios.create({ baseURL: "https://api.allthesports.info/" });

const ProviderPlayerPerSport = () => {
  const [data, setData] = useState([]);  
  const [loaded, setLoaded] = useState(false);
  useEffect(() => {
    const fetchData = async () => {
      await client
        .get('sports').then(async (res) => {
          const apiData = res.data;
          const result = [];
          for (let i = 0; i < apiData.length; i++) {
            await client.get('players/filter?filter_type=sport&filter_value=' + apiData[i].name).then(async (res2) => {
              result.push({category: apiData[i].name, count: res2.data.length})
            })
          }
          setData(result);
          setLoaded(true);
        });
    };

    if (!loaded) {
      fetchData();
    }
  }, [data]);
    return (
      <ResponsiveContainer width="100%" height={300}>
        <RadarChart cx="50%" cy="50%" outerRadius="80%" data={data}>
          <PolarGrid />
          <PolarAngleAxis dataKey="category" />
          <PolarRadiusAxis />
          <Radar name="Mike" dataKey="count" stroke="#8884d8" fill="#8884d8" fillOpacity={0.6} />
        </RadarChart>
      </ResponsiveContainer>
    );
}

export default ProviderPlayerPerSport;