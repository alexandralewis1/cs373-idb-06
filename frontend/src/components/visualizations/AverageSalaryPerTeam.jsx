import React, { useRef, useEffect, useState } from "react";
import * as d3 from "d3";
import axios from 'axios';

const client = axios.create({ baseURL: import.meta.env.VITE_API_URL });

const AverageSalaryPerTeam = () => {
  const ref = useRef(null);
  //Number of teams return 
  const [data, setData] = useState([]);
  const [loaded, setLoaded] = useState(false);


  useEffect(() => {

    
    const fetchData = async () => {

      await client
      .get('v1/json/teams', {
        headers: {
          'Access-Control-Allow-Origin': '*',
        },
      })
      .then(async (response) => {
        const teams = response.data.data
        var teamToColor = {}
        for(let i = 0; i < teams.length; i++) {
          teamToColor[teams[i].abbreviation] = {pcolor: teams[i].primarycolor, scolor: teams[i].secondarycolor, tcolor: teams[i].tertiarycolor, qcolor: teams[i].quaternarycolor,
        }
      }
        await client
        .get('v1/json/players', {
          headers: {
            'Access-Control-Allow-Origin': '*',
          },
        })
        .then((response) => {
          const apiData = response.data.data


          const result = {};

          for (let i = 0; i < apiData.length; i++) {
            var team = apiData[i].teamshort;
            if(apiData[i].salary !== null) {
              if (result[team]) {
                result[team].total = result[team].total + apiData[i].salary;
                result[team].count += 1
              } else {
                result[team] = {total: apiData[i].salary, count: 1}
              }
            }
            
          }
          const countsArr = Object.keys(result).map((key) => {
            return Object.assign({team: key, average: result[key].total/result[key].count}, teamToColor[key]);
          });
          
          const sortedData = countsArr.sort((a, b) => b.average - a.average);
          // // Get top 40 countries by count
          // const top40Data = sortedData.slice(0, 50);

          // console.log(countsArr)
          setData(sortedData);
          setLoaded(true);
        });
      });
    
    };
    if(!loaded) {
      fetchData();
    }
    else {
      const svg = d3.select(ref.current);
      const margin = { top: 120, right: 100, bottom: 80, left: 100 };
      const width = svg.attr("width") - margin.left - margin.right;
      const height = svg.attr("height") - margin.top - margin.bottom;

      const patterns = svg.selectAll("pattern")
        .data(data)
        .enter()
        .append("pattern")
        .attr("id", (d) => `${d.team}-pattern`)
        .attr("patternUnits", "userSpaceOnUse")
        .attr("width", 8)
        .attr("height", 8)
        .attr("patternTransform", "rotate(45)")

      patterns.append("rect")
        .attr("x", 0)
        .attr("y", 0)
        .attr("width", 2)
        .attr("height", 8)
        .style("fill", (d) => `#${d.pcolor}`);

      patterns.append("rect")
        .attr("x", 2)
        .attr("y", 0)
        .attr("width", 2)
        .attr("height", 8)
        .style("fill", (d) => `#${d.scolor}`);

      patterns.append("rect")
        .attr("x", 4)
        .attr("y", 0)
        .attr("width", 2)
        .attr("height", 8)
        .style("fill", (d) => `#${d.tcolor}`);

      patterns.append("rect")
        .attr("x", 6)
        .attr("y", 0)
        .attr("width", 2)
        .attr("height", 8)
        .style("fill", (d) => `#${d.qcolor}`);

      const x = d3
        .scaleBand()
        .range([0, width])
        .padding(0.33)
        .domain(data.map((d) => d.team));

      const y = d3
        .scaleLinear()
        .range([height, 0])
        .domain([0, d3.max(data, (d) => d.average)]);

      const g = svg
        .append("g")
        .attr("transform", `translate(${margin.left},${margin.top})`);

      g.append("text")
        .attr("class", "graph-title")
        .attr("x", width / 2)
        .attr("y", 0 - (margin.top / 8))
        .style("text-anchor", "middle")
        .style("font-size", "22px") // increase font size to 24 pixels
        .style("text-decoration", "underline") // add underline
        .style("font-weight", "bold")
        .text("Average Salary Per Team");

      g.append("g")
        .attr("class", "axis axis-x")
        .attr("transform", `translate(0,${height})`)
        .call(d3.axisBottom(x));

      g.append("text")
        .attr("class", "axis-label")
        .attr("x", width / 2)
        .attr("y", height + 40)
        .style("text-anchor", "middle")
        .text("Teams");

      g.append("g")
        .attr("class", "axis axis-y")
        .call(d3.axisLeft(y)
        .tickFormat(d3.format(","))
        .ticks(10, "-"))

      g.append("text")
        .attr("class", "axis-label")
        .attr("transform", "rotate(-90)")
        .attr("y", 0 - margin.left)
        .attr("x", 0 - (height / 2))
        .attr("dy", "1em")
        .style("text-anchor", "middle")
        .text("Average Salary ($)");
      
      g.selectAll(".bar")
        .data(data)
        .enter()
        .append("rect")
        .attr("class", "bar")
        .attr("x", (d) => x(d.team))
        .attr("y", (d) => y(d.average))
        .attr("width", x.bandwidth())
        .attr("height", (d) => height - y(d.average))
        .style("fill", (d) => `url(#${d.team}-pattern)`);
        // .style("fill", (d) => `#${d.color}`);
    }
  }, [data]);

  return (
    <>
        <svg ref={ref} width={1240} height={720}>
        </svg>
    </>
  );
};
export default AverageSalaryPerTeam;
