
import React, { useEffect, useState } from "react";
import { PieChart, Pie, Legend, Tooltip, ResponsiveContainer } from 'recharts';
import axios from 'axios';
const client = axios.create({ baseURL: "https://api.allthesports.info/" });

const ProviderAverageLeaguePerCountry = () => {
  const [data, setData] = useState([]);  
  const [loaded, setLoaded] = useState(false);
  useEffect(() => {
    const fetchData = async () => {
      await client
        .get('leagues').then(async (league) => {
          const apiData = league.data;
          const result = [];
          for (let i = 0; i < apiData.length; i++) {
            if (apiData[i].country != "No information about host coun") {
              const country = apiData[i].country;
              const index = result.findIndex(obj => obj.country === country);
  
              if (index === -1) {
                // If the country is not already in the result array, add it with count = 1
                result.push({ country, count: 1 });
              } else {
                // If the country is already in the result array, increment its count
                result[index].count++;
              }
            }
          }
          setData(result);
          setLoaded(true);
        });
    };

    if (!loaded) {
      fetchData();
    }
  }, [data]);

  let renderLabel = function(entry) {
      return entry.country;
  }
    return (
      <ResponsiveContainer width="100%" height={400}>
        <PieChart>
          <Pie
            dataKey="count"
            isAnimationActive={false}
            data={data}
            cx="50%"
            cy="50%"
            outerRadius={100}
            fill="#8884d8"
            label={renderLabel}
          />
        </PieChart>
      </ResponsiveContainer>
    );
}

export default ProviderAverageLeaguePerCountry;