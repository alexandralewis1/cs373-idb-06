import React, { useRef, useEffect, useState } from "react";
import * as d3 from "d3";
import axios from 'axios';
import Spinner from 'react-bootstrap/Spinner';

const countryCodes = {
  "Lithuania": "LTU",
  "Yugoslavia": "YUG",
  "Switzerland": "CHE",
  "Bosnia and Herzegovina": "BIH",
  "USA": "USA",
  "Spain": "ESP",
  "Latvia": "LVA",
  "Serbia": "SRB",
  "Croatia": "HRV",
  "Slovenia": "SVN",
  "Czech Republic": "CZE",
  "Turkey": "TUR",
  "England": "GBR",
  "France": "FRA",
  "Greece": "GRC",
  "Congo": "COD",
  "Germany": "DEU",
  "Canada": "CAN",
  "Australia": "AUS",
  "Brazil": "BRA",
  "Macedonia": "MKD",
  "Bahamas": "BHS",
  "Austria": "AUT",
  "Cameroon": "CMR",
  "Finland": "FIN",
  "Belgium": "BEL",
  "United Kingdom": "GBR",
  "St. Lucia": "LCA",
  "Nigeria": "NGA",
  "Ukraine": "UKR",
  "Japan": "JPN",
  "Sudan": "SDN",
  "Angola": "AGO",
  "Israel": "ISR",
  "Guinea": "GIN",
  "Jamaica": "JAM",
  "Montenegro": "MNE",
  "New Zealand": "NZL",
  "Senegal": "SEN",
  "Georgia": "GEO",
  "Dominican Republic": "DOM",
  "Italy": "ITA",
  "DR Congo": "COD",
  "Portugal": "PRT"
};

const client = axios.create({ baseURL: import.meta.env.VITE_API_URL });

const PlayersPerCountry = () => {
  const ref = useRef(null);
  //Number of teams return 
  const [data, setData] = useState([]);
  const [loaded, setLoaded] = useState(false);
  useEffect(() => {

    const fetchData = async () => {
      await client
        .get('v1/json/players', {
          headers: {
            'Access-Control-Allow-Origin': '*',
          },
        })
        .then((response) => {
          const apiData = response.data.data
          const result = {};

          for (let i = 0; i < apiData.length; i++) {
            var country = countryCodes[apiData[i].country];
            if (countryCodes[apiData[i].country] === undefined) {
              country = apiData[i].country
            }
            if (result[country]) {
              result[country]++;
            } else {
              result[country] = 1;
            }
          }
          if (loaded) {
             return;
          }
           
          const countsArr = Object.keys(result).map((key) => {
            return { country: key, count: result[key], color: '#' + Math.floor(Math.random() * 16777215).toString(16)};
          });
        
          let sortedData = countsArr.sort((a, b) => b.count - a.count);

          // // Get top 40 countries by scount
          sortedData = sortedData.slice(0, 20);

          setData(sortedData);
          setLoaded(true);
    
        });
    };
    if(!loaded) {
       fetchData();
    }
    else {
      const svg = d3.select(ref.current);
      const width = svg.attr('width');
      const height = svg.attr('height');
      const margin = { top: 100, right: 40, bottom: 80, left: 40 };
      const chartWidth = width - margin.left - margin.right;
      const chartHeight = height - margin.top - margin.bottom;
      const radius = Math.min(chartWidth, chartHeight) / 2;
      const holeRadius = radius / 2; // radius of the hole in the center of the chart
      const colors = data.map(d => d.color);
  
  const arc = d3.arc()
    .innerRadius(holeRadius)
    .outerRadius(radius);

  const pie = d3.pie()
    .value(d => d.count)

  const arcs = pie(data);

  const colorScale = d3.scaleOrdinal()
    .domain(colors)
    .range(colors);

  svg.selectAll('*').remove();

  svg.append('text') // add a title to the pie chart
    .attr("class", "graph-title")
    .attr('x', width / 2)
    .attr('y', margin.top / 2)
    .attr('text-anchor', 'middle')
    .style("font-size", "22px") // increase font size to 24 pixels
    .style("text-decoration", "underline") // add underline
    .style("text-decoration", "bold") // add underline
    .style("font-weight", "bold")
    .text('Number of Players Per Country - Top 20');

  const arcLabel = d3.arc().outerRadius(radius).innerRadius(radius - 80);


  const labelGroup = svg.append('g')
    .attr('transform', `translate(${width / 2}, ${height / 2})`);

  const paths = svg.select('g')
    .selectAll('path')
    .data(arcs)
    .enter()
    .append('path')
    .attr('fill', d => colorScale(d.data.color))
    .attr('d', arc);

  // add legend
  const legend = svg.append('g')
    .attr('class', 'legend')
    .attr('transform', `translate(${width - margin.right - 70},${margin.top})`)
    .selectAll('.legend-item')
    .data(data)
    .enter()
    .append('g')
    .attr('class', 'legend-item')
    .attr('transform', (d, i) => `translate(0, ${i * 20})`);

  legend.append('rect')
    .attr('x', 0)
    .attr('y', 0)
    .attr('width', 10)
    .attr('height', 10)
    .attr('fill', d => colorScale(d.color));

  legend.append('text')
    .attr('x', 15)
    .attr('y', 10)
    .text(d => d.country + " - " + d.count);

  }
}, [data]);

return <svg ref={ref} width={1000} height={520}></svg>;
};

function midAngle(d) {
  return d.startAngle + (d.endAngle - d.startAngle) / 2;
}
export default PlayersPerCountry;
