import Card from 'react-bootstrap/Card';
import PropTypes from 'prop-types';

const DeveloperCard = (props) => {
  return (
    <Card style={{ width: '14rem' }}>
      <Card.Img variant="top" src={props.devInfo.image} />
      <Card.Body>
        <Card.Title>{props.devInfo.name}</Card.Title>

        <Card.Subtitle>@{props.devInfo.gitlab_username}</Card.Subtitle>
        <Card.Text>Role: {props.devInfo.role}</Card.Text>
        <Card.Text>{props.devInfo.bio}</Card.Text>

        <Card.Footer className="text-muted">
          Commits: {props.devInfo.commits} <br />
          Issues: {props.devInfo.issues} <br />
          Unit Tests: {props.devInfo.unit_tests}
        </Card.Footer>
      </Card.Body>
    </Card>
  );
};

DeveloperCard.propTypes = {
  devInfo: PropTypes.shape({
    name: PropTypes.string.isRequired,
    gitlab_username: PropTypes.string.isRequired,
    role: PropTypes.string.isRequired,
    bio: PropTypes.string.isRequired,
    commits: PropTypes.number.isRequired,
    issues: PropTypes.number.isRequired,
    unit_tests: PropTypes.number.isRequired,
    image: PropTypes.string,
  }),
};

export default DeveloperCard;
