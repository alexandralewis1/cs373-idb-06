// <reference types="vitest" />
import { defineConfig } from 'vite';
import react from '@vitejs/plugin-react';

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react()],
  test: {
    globals: true,
    environment: "happy-dom"
  },
  html: {
    title: "NBADb",
    icon: "/frontend/src/assets/favicon.ico", 
  }
});