import unittest
from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

# Based on code from https://gitlab.com/10AMGroup11/bookrus/-/blob/main/frontend/guitests.py

URL = "https://nbadb.me/"

class TestFrontendGui(unittest.TestCase):

  # Navbar Tests

  @classmethod
  def setUpClass(self) -> None:
    options = webdriver.ChromeOptions()
    options.add_experimental_option('excludeSwitches', ['enable-logging'])
    options.add_argument("--headless")
    options.add_argument("--no-sandbox")
    options.add_argument("--disable-dev-shm-usage")
    chrome_prefs = {}
    options.experimental_options["prefs"] = chrome_prefs
    # Disable images
    chrome_prefs["profile.default_content_settings"] = {"images": 2}

    self.driver = webdriver.Chrome(options=options, service=Service(ChromeDriverManager().install()))
   
    self.driver.get(URL)
    

  @classmethod
  def tearDownClass(self):
    self.driver.quit()

  def test_Brand(self):
    element = self.driver.find_element(By.CLASS_NAME, 'navbar-brand')
    element.click()
    self.assertEqual(self.driver.current_url, URL)
  

  def test_Home(self):
    try:
      WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.CLASS_NAME, 'navbar-brand')))
      element = self.driver.find_element(By.CLASS_NAME, 'navbar-brand')
      element.click()
    except Exception as ex:
      print("Couldn't find navbar brand: " + str(ex))

    try:
      WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div/div/nav/div/div/div/a[1]')))
      element = self.driver.find_element(By.XPATH, '/html/body/div/div/nav/div/div/div/a[1]')
      element.click()
    except Exception as ex:
      print("Couldn't find Home link: " + str(ex))
    
    self.assertEqual(self.driver.current_url, URL)


  def test_About(self):
    try:
      WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.CLASS_NAME, 'navbar-brand')))
      element = self.driver.find_element(By.CLASS_NAME, 'navbar-brand')
      element.click()
    except Exception as ex:
      print("Couldn't find navbar brand: " + str(ex))

    try:
      WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div/div/nav/div/div/div/a[2]')))
      element = self.driver.find_element(By.XPATH, '/html/body/div/div/nav/div/div/div/a[2]')
      element.click()
    except Exception as ex:
      print("Couldn't find About link: " + str(ex))

    self.assertEqual(self.driver.current_url, URL + "about/")

  def test_Players(self):
    try:
      WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.CLASS_NAME, 'navbar-brand')))
      element = self.driver.find_element(By.CLASS_NAME, 'navbar-brand')
      element.click()
    except Exception as ex:
      print("Couldn't find navbar brand: " + str(ex))

    try:
      WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div/div/nav/div/div/div/a[3]')))
      element = self.driver.find_element(By.XPATH, '/html/body/div/div/nav/div/div/div/a[3]')
      element.click()
    except Exception as ex:
      print("Couldn't find Players link: " + str(ex))

    self.assertEqual(self.driver.current_url, URL + "players/")

  def test_Teams(self):
    try:
      WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.CLASS_NAME, 'navbar-brand')))
      element = self.driver.find_element(By.CLASS_NAME, 'navbar-brand')
      element.click()
    except Exception as ex:
      print("Couldn't find navbar brand: " + str(ex))

    try:
      WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div/div/nav/div/div/div/a[4]')))
      element = self.driver.find_element(By.XPATH, '/html/body/div/div/nav/div/div/div/a[4]')
      element.click()
    except Exception as ex:
      print("Couldn't find Teams link: " + str(ex))

    self.assertEqual(self.driver.current_url, URL + "teams/")
  
  def test_Arenas(self):
    try:
      WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.CLASS_NAME, 'navbar-brand')))
      element = self.driver.find_element(By.CLASS_NAME, 'navbar-brand')
      element.click()
    except Exception as ex:
      print("Couldn't find navbar brand: " + str(ex))

    try:
      WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div/div/nav/div/div/div/a[5]')))
      element = self.driver.find_element(By.XPATH, '/html/body/div/div/nav/div/div/div/a[5]')
      element.click()
    except Exception as ex:
      print("Couldn't find Arenas link: " + str(ex))

    self.assertEqual(self.driver.current_url, URL + "arenas/")


  # Instance Tests
  def test_player(self):
    self.driver.get(URL + "players")
    self.assertEqual(self.driver.current_url, URL + "players/")
  
  def test_team(self):
    self.driver.get(URL + "teams")
    self.assertEqual(self.driver.current_url, URL + "teams/")

  def test_arena(self):
    self.driver.get(URL + "arenas")
    self.assertEqual(self.driver.current_url, URL + "arenas/")


  # Splash Tests
  def test_aboutSplashLink(self):
    # Wait for the navbar-brand to load, then click to go back to the home screen
    WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.CLASS_NAME, 'navbar-brand')))
    element = self.driver.find_element(By.CLASS_NAME, 'navbar-brand')
    element.click()

    # Wait for splash card to load, then click
    WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div/div/a')))
    element = self.driver.find_element(By.XPATH, '//*[@id="root"]/div/div/a')
    self.driver.execute_script("arguments[0].click();", element)
    self.assertEqual(self.driver.current_url, URL + "about/")


if __name__ == '__main__':
    unittest.main()