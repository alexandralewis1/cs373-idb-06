import React from 'react';
import renderer from 'react-test-renderer';
import { MemoryRouter } from 'react-router-dom';

import About from '../src/pages/about.jsx';
import Player from '../src/pages/player.jsx';
import Arena from '../src/pages/arena.jsx';
import Landing from '../src/pages/landing.jsx';
import Team from '../src/pages/team.jsx';
import App from '../src/App.jsx';
import Navbar from '../src/components/Navbar.jsx';
import PlayerCard from '../src/components/PlayerCard.jsx';
import ArenaCard from '../src/components/ArenaCard.jsx';
import TeamCard from '../src/components/TeamCard.jsx';

// start making tests

test('App Initialization', () => {
  const component = renderer.create(<App />);
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});

test('About Initialization', () => {
  const component = renderer.create(
    <MemoryRouter>
      <About />
    </MemoryRouter>,
  );
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});

test('Player Initialization', () => {
  const component = renderer.create(
    <MemoryRouter>
      <Player />
    </MemoryRouter>,
  );
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});

test('Arena Initialization', () => {
  const component = renderer.create(
    <MemoryRouter>
      <Arena />
    </MemoryRouter>,
  );
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});

test('Landing Initialization', () => {
  const component = renderer.create(
    <MemoryRouter>
      <Landing />
    </MemoryRouter>,
  );
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});

test('Team Initialization', () => {
  const component = renderer.create(
    <MemoryRouter>
      <Team />
    </MemoryRouter>,
  );
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});

test('Navbar Initialization', () => {
  const component = renderer.create(<Navbar />);
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});

test('Player Card Initialization', () => {
  const props = {
    photo: 'photo',
    first: 'first',
    last: 'last',
    teamshort: 'teamshort',
    tid: 100,
    country: 'country',
    salary: 1,
    position: 'position',
    id: 9,
    birthday: 'birthday',
  };
  const component = renderer.create(
    <MemoryRouter>
      <PlayerCard player={props} highlight="highlight" />
    </MemoryRouter>,
  );
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});

test('Team Card Initialization', () => {
  const props = {
    logo: 'logo',
    teamname: 'teamname',
    abbreviation: 'abbreviation',
    city: 'city',
    conference: 'conference',
    division: 'division',
    tid: 1,
    active: true,
  };
  const component = renderer.create(
    <MemoryRouter>
      <TeamCard team={props} highlight="highlight" />
    </MemoryRouter>,
  );
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});

test('Arena Card Initialization', () => {
  const props = {
    arenaname: 'Madison Square Garden',
    capacity: 20789,
    city: 'New York',
    state: 'NY',
    country: 'USA',
    active: false,
    aid: '1',
  };
  const component = renderer.create(
    <MemoryRouter>
      <ArenaCard arena={props} highlight="highlight" />
    </MemoryRouter>,
  );
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});
