# cs373-idb-06

## IDB group: 6

## Project Name: NBADB

## Group members: Alexandra Lewis, Ayush Patel, Aditya Agrawal, Kelly Thai, Mason Wilderom

## Description
Our website will connect NBA fans to information about their favorite players, teams, and the stadiums where the games are played. 


## API's

- [ ] Players (https://api.sportsdata.io/v3/nba/scores/json/Players/%7Bteam%7D)
- [ ] Teams (https://rapidapi.com/api-sports/api/api-nba/)
- [ ] Stadiums (https://api.sportsdata.io/api/nba/odds/json/Stadiums, https://developer.ticketmaster.com/products-and-docs/apis/getting-started/)

Also: https://en.wikipedia.org/api/rest_v1/ for images & blurbs 



## 3 Models

- [ ] Players 
  - Instances: 450
  - Attributes: name, salary, country of origin, position, team
  - Media: photos, team logo 
  - Connection to Other Models: players on teams, players play in stadiums


- [ ] Teams
  - Instances: 32
  - Attributes: Names, abbreviation, city, conference, win record
  - Media: logo, photo of their city
  - Connection to Other Models: teams are associated with stadiums and have players 

- [ ] Arenas
  - Instances: 100
  - Attributes: name, state, country, capacity, active 
  - Media: google maps map, image of stadium 
  - Connection to Other Models: players play at stadiums and are on teams 



## Organizational Technique
- [ ] Traditional organization. 


## Questions Our Site Will Answer:
- [ ] Where does an NBA team play / where have they played?
- [ ] What players are in the NBA? 
- [ ] What are this players statistics? 
- [ ] What games have been played at a stadium near me? 
- [ ] What are teams' win records? 
