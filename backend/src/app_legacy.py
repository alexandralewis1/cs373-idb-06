from flask import request, jsonify, send_file, Response
from models import Players, Teams, Arenas, db, app
from schema import players_schema, teams_schema, arenas_schema
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import or_
import json

NORMAL_PAGE_SIZE = 20
# conn = psycopg2.connect(host='database-1.cn0cmdprv9xo.us-east-1.rds.amazonaws.com', database='nbadb', user='postgres', password='%C!c8TYnu%hm8*', port=5432)


@app.route("/")
def index():
    return "Hello World!"


@app.route("/v1/json/players")
def players():
    page = request.args.get("offset", type=int)
    perPage = request.args.get("limit", type=int)
    search = request.args.get("search")
    teamshort = request.args.get("teamshort")
    query = db.session.query(Players)
    count = query.count()
    if search is not None:
        results = []
        search = search.split("+")
        for term in search:
            results.append(Players.first.ilike("%" + term + "%"))
            results.append(Players.last.ilike("%" + term + "%"))
            results.append(Players.position.ilike("%" + term + "%"))
            results.append(Players.teamshort.ilike("%" + term + "%"))
            results.append(Players.city.ilike("%" + term + "%"))
            results.append(Players.country.ilike("%" + term + "%"))
            results.append(Players.college.ilike("%" + term + "%"))
        query = query.filter(or_(*results))
        count = query.count()
    if page is not None:
        query = paginate(query, page, perPage)
    result = players_schema.dump(query, many=True)
    return jsonify({"data": result, "total": count})


@app.route("/v1/json/teams")
def teams():
    page = request.args.get("offset", type=int)
    perPage = request.args.get("limit", type=int)
    search = request.args.get("search")
    query = db.session.query(Teams)
    count = query.count()
    if search is not None:
        results = []
        search = search.split("+")
        for term in search:
            results.append(Teams.abbreviation.ilike("%" + term + "%"))
            results.append(Teams.city.ilike("%" + term + "%"))
            results.append(Teams.teamname.ilike("%" + term + "%"))
            results.append(Teams.conference.ilike("%" + term + "%"))
            results.append(Teams.division.ilike("%" + term + "%"))
            results.append(Teams.twitter.ilike("%" + term + "%"))
        query = query.filter(or_(*results))
        count = query.count()
    if page is not None:
        query = paginate(query, page, perPage)
    result = teams_schema.dump(query, many=True)
    return jsonify({"data": result, "total": count})
    # return send_file('../raw/players.json')


@app.route("/v1/json/arenas")
def arenas():
    page = request.args.get("offset", type=int)
    perPage = request.args.get("limit", type=int)
    search = request.args.get("search")
    query = db.session.query(Arenas)
    count = query.count()
    if search is not None:
        results = []
        search = search.split("+")
        for term in search:
            results.append(Arenas.arenaname.ilike("%" + term + "%"))
            results.append(Arenas.address.ilike("%" + term + "%"))
            results.append(Arenas.city.ilike("%" + term + "%"))
            results.append(Arenas.state.ilike("%" + term + "%"))
            results.append(Arenas.country.ilike("%" + term + "%"))
        query = query.filter(or_(*results))
        count = query.count()
    if page is not None:
        query = paginate(query, page, perPage)
    result = arenas_schema.dump(query, many=True)
    return jsonify({"data": result, "total": count})
    # return send_file('../raw/players.json')

@app.route("/v1/json/players/<int:pid>")
def player(pid):
    query = db.session.query(Players).filter_by(id = pid)
    try:
        result = players_schema.dump(query, many=True)
    except IndexError:
        return return_error(f"Invalid player ID: {pid}")
    return jsonify({"data": result, "total": 1})

@app.route("/v1/json/teams/<int:tid>")
def team(tid):
    query = db.session.query(Teams).filter_by(tid = tid)
    try:
        result = teams_schema.dump(query, many=True)
    except IndexError:
        return return_error(f"Invalid team ID: {tid}")
    return jsonify({"data": result, "total": 1})

@app.route("/v1/json/arenas/<int:aid>")
def arena(aid):
    query = db.session.query(Arenas).filter_by(aid = aid)
    try:
        result = arenas_schema.dump(query, many=True)
    except IndexError:
        return return_error(f"Invalid arena ID: {aid}")
    return jsonify({"data": result, "total": 1})

# GET /api/stats/
@app.route("/api/stats/", methods=["GET"])
def get_stats():
    # query the db for stats
    ## formating teh data
    # return the data
    return jsonify({"stats": "stats"})


def paginate(query, page_num, num_per_page):
    if num_per_page is None:
        num_per_page = NORMAL_PAGE_SIZE
    return query.paginate(page=page_num, per_page=num_per_page, error_out=False).items

def return_error(msg):
    resp = Response(json.dumps({"error": msg}), mimetype="application/json")
    resp.error_code = 404
    return resp

if __name__ == "__main__":
    app.run()
