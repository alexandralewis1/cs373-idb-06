from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS

app = Flask(__name__)
CORS(app)
app.debug = True
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
app.config[
    "SQLALCHEMY_DATABASE_URI"
] = "postgresql://postgres:%C!c8TYnu%hm8*@database-1.cn0cmdprv9xo.us-east-1.rds.amazonaws.com:5432/nbadb"
db = SQLAlchemy(app)

# {
#   "data": [
#     {
#       "pid": 190183,
#       "first": "Luka",
#       "last": "Doncic",
#       "tid": 25,
#       "team": "DAL",
#       "status": "active",
#       "jersey": 77,
#       "position": "PG",
#       "height": 79,
#       "weight": 230,
#       "dob": "1999-02-28T00:00:00",
#       "city": "Ljubljana",
#       "state": null,
#       "country": "Slovenia",
#       "education": null,
#       "salary": 37095000,
#       "photo": "https://s3-us-west-2.amazonaws.com/static.fantasydata.com/headshots/nba/low-res/20001984.png",
#       "experience": 5
#     }
#   ],
#   "total": 1
# }


class Players(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    tid = db.Column(db.Integer)
    teamshort = db.Column(db.String(3))
    jersey = db.Column(db.Integer)
    position = db.Column(db.String(2))

    # set these to 60, idk if it needs to be changed
    first = db.Column(db.String(60))
    last = db.Column(db.String(60))

    height = db.Column(db.Integer)
    weight = db.Column(db.Integer)
    birthday = db.Column(db.String(20))

    # random again
    city = db.Column(db.String(50))

    country = db.Column(db.String(50))
    college = db.Column(db.String(50))
    salary = db.Column(db.Integer)

    # random again
    photo = db.Column(db.String(200))

    experience = db.Column(db.Integer)
    twitter = db.Column(db.String(100))

    # tags = db.relationship('Tag', secondary = city_tag_link, backref = 'cities')
    # apartments = db.relationship('Apartment', backref = 'city')
    # jobs = db.relationship('Job', backref = 'city')


# {
#   "data": [
#     {
#       "tid": 25,
#       "abbreviation": "DAL",
#       "active" : true
#       "city": "Dallas",
#       "name": "Mavericks",
#       "aid": 25,
#       "conference": "Western",
#       "division": "Southwest",
#       "primary": "0064B1",
#       "secondary": "00285E",
#       "tertiary": "BBC4CA",
#       "quaternary": "000000",
#       "logo": "https://upload.wikimedia.org/wikipedia/en/9/97/Dallas_Mavericks_logo.svg"
#       "nbadotcomteamid": 1610612764
#     }
#   ],
#   "total": 1
# }


class Teams(db.Model):
    tid = db.Column(db.Integer, primary_key=True)
    abbreviation = db.Column(db.String(3))
    active = db.Column(db.Boolean)
    # set these to 50, idk if it needs to be changed
    city = db.Column(db.String(50))
    teamname = db.Column(db.String(50))
    aid = db.Column(db.Integer)

    # set these to 60, idk if it needs to be changed
    conference = db.Column(db.String(60))
    division = db.Column(db.String(60))

    primarycolor = db.Column(db.String(6))
    secondarycolor = db.Column(db.String(6))
    tertiarycolor = db.Column(db.String(6))

    # random again
    quaternarycolor = db.Column(db.String(6))
    logo = db.Column(db.String(200))
    nbadotcomteamid = db.Column(db.Integer)
    twitter = db.Column(db.String(100))
    championships = db.Column(db.Integer)
    founded = db.Column(db.Integer)


# {
#   "data": [
#     {
#       "aid": 25,
#       "active", true
#       "name": "American Airlines Center",
#       "address": "2500 Victory Ave.",
#       "city": "Dallas",
#       "state": "Texas",
#       "zip": "75219",
#       "country": "USA",
#       "capacity": 19200,
#       "lat": 32.790556,
#       "long": -96.810278
#     }
#   ],
#   "total": 1
# }
class Arenas(db.Model):
    aid = db.Column(db.Integer, primary_key=True)
    active = db.Column(db.Boolean)
    # set these to 100, idk if it needs to be changed
    arenaname = db.Column(db.String(100))

    # set these to 50, idk if it needs to be changed
    address = db.Column(db.String(50))

    city = db.Column(db.String(50))
    state = db.Column(db.String(50))
    zip = db.Column(db.String(50))

    # set these to 60, idk if it needs to be changed
    country = db.Column(db.String(60))

    capacity = db.Column(db.Integer)
    lat = db.Column(db.Integer)
    long = db.Column(db.Integer)

    tid = db.Column(db.Integer)
    image = db.Column(db.String(150))
