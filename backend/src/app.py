from flask import request, jsonify, Response
from models import Players, Teams, Arenas, db, app
from schema import players_schema, teams_schema, arenas_schema
from sqlalchemy import or_
import json

# the default number of entries on a page
DEFAULT_PAGE_SIZE = 20

# shared portion of API URL
URL_PREFIX = "/v1/json/"

@app.route("/")
def index() :
    """
    hello world function for testing
    
    :return: hopefully code 200
    """
    return "Hello World! v2"

@app.route(URL_PREFIX + "players")
def players() :
    """
    query entire players table
    supports filter, search, sort, pagination
    
    :return: json file
    """

    # search columns
    search = request.args.get("search", type=str)

    # filter by field
    teamshort = request.args.get("teamshort", type=str)
    position = request.args.get("position", type=str)
    country = request.args.get("country", type=str)
    height = request.args.get("height", type=int)
    weight = request.args.get("weight", type=int)
    salary = request.args.get("salary", type=int)

    # filter by range (accompanies height, weight, salary)
    min = request.args.get("min", type=int)
    max = request.args.get("max", type=int)

    # sort query
    sort = request.args.get("sort", type=str)
    
    # pagination queries
    page = request.args.get("offset", type=int)
    page_size = request.args.get("limit", type=int)

    # start with the set of all players
    query = db.session.query(Players)

    # narrow to results of column search
    if search is not None :
        results = []
        search = search.split()
        for term in search:
            results.append(Players.first.ilike("%" + term + "%"))
            results.append(Players.last.ilike("%" + term + "%"))
            results.append(Players.position.ilike("%" + term + "%"))
            results.append(Players.teamshort.ilike("%" + term + "%"))
            results.append(Players.city.ilike("%" + term + "%"))
            results.append(Players.country.ilike("%" + term + "%"))
            results.append(Players.college.ilike("%" + term + "%"))
        query = query.filter(or_(*results))

    # apply specific field filters
    if min is None :
        min = float("-inf")
    if max is None :
        max = float("inf")

    if teamshort is not None :
        query = query.filter(Players.teamshort.ilike(teamshort))
    if position is not None :
        query = query.filter(Players.position.ilike(position))
    if country is not None :
        query = query.filter(Players.country.ilike(country))
    if height is not None :
        query = query.filter(Players.height.between(min, max))
    if weight is not None :
        query = query.filter(Players.weight.between(min, max))
    if salary is not None :
        query = query.filter(Players.salary.between(min, max))

    # sort the query
    if sort is not None :
        if getattr(Players, sort, None) is not None :
            query = query.order_by(getattr(Players, sort))

    # paginate
    total = query.count()
    if page is not None :
        query = paginate(query, page, page_size)

    # generate output
    result = players_schema.dump(query, many=True)
    return jsonify({"data": result, "total": total})

@app.route(URL_PREFIX + "players/<int:pid>")
def player(pid) :
    """
    players table lookup by player id
    
    :param pid: player id
    :return: json file
    """
    query = db.session.query(Players).filter_by(id = pid)
    try :
        result = players_schema.dump(query, many=True)
    except IndexError :
        return return_error(f"Invalid player ID: {pid}")
    return jsonify({"data": result, "total": 1})
    
@app.route(URL_PREFIX + "teams")
def teams() :
    """
    query entire teams table
    supports filter, search, sort, pagination
    
    :return: json file
    """

    # search columns
    search = request.args.get("search", type=str)

    # filter by field
    conference = request.args.get("conference", type=str)
    division = request.args.get("division", type=str)

    # sort query
    sort = request.args.get("sort", type=str)
    
    # pagination queries
    page = request.args.get("offset", type=int)
    page_size = request.args.get("limit", type=int)

    # start with the set of all teams
    query = db.session.query(Teams)

    # narrow to results of column search
    if search is not None :
        results = []
        search = search.split()
        for term in search:
            results.append(Teams.abbreviation.ilike("%" + term + "%"))
            results.append(Teams.city.ilike("%" + term + "%"))
            results.append(Teams.teamname.ilike("%" + term + "%"))
            results.append(Teams.conference.ilike("%" + term + "%"))
            results.append(Teams.division.ilike("%" + term + "%"))
            results.append(Teams.twitter.ilike("%" + term + "%"))
        query = query.filter(or_(*results))

    if conference is not None :
        query = query.filter(Teams.conference.ilike(conference))
    if division is not None :
        query = query.filter(Teams.division.ilike(division))

    # sort the query
    if sort is not None :
        if getattr(Teams, sort, None) is not None :
            query = query.order_by(getattr(Teams, sort))

    # paginate
    total = query.count()
    if page is not None :
        query = paginate(query, page, page_size)

    # generate output
    result = teams_schema.dump(query, many=True)
    return jsonify({"data": result, "total": total})

@app.route(URL_PREFIX + "teams/<int:tid>")
def team(tid) :
    """
    teams table lookup by team id
    
    :param tid: team id
    :return: json file
    """
    query = db.session.query(Teams).filter_by(tid = tid)
    try:
        result = teams_schema.dump(query, many=True)
    except IndexError:
        return return_error(f"Invalid team ID: {tid}")
    return jsonify({"data": result, "total": 1})

@app.route(URL_PREFIX + "arenas")
def arenas() :
    """
    query entire arenas table
    supports filter, search, sort, pagination
    
    :return: json file
    """

    # search columns
    search = request.args.get("search", type=str)

    # filter by field
    capacity = request.args.get("capacity", type=int)
    city = request.args.get("city", type=str)
    state = request.args.get("state", type=str)
    country = request.args.get("country", type=str)
    active = request.args.get("active", type=bool)

    # filter by range (accompanies height, weight, salary)
    min = request.args.get("min", type=int)
    max = request.args.get("max", type=int)

    # sort query
    sort = request.args.get("sort", type=str)
    
    # pagination queries
    page = request.args.get("offset", type=int)
    page_size = request.args.get("limit", type=int)

    # start with the set of all arenas
    query = db.session.query(Arenas)

    # narrow to results of column search
    if search is not None :
        results = []
        search = search.split()
        for term in search:
            results.append(Arenas.arenaname.ilike("%" + term + "%"))
            results.append(Arenas.address.ilike("%" + term + "%"))
            results.append(Arenas.city.ilike("%" + term + "%"))
            results.append(Arenas.state.ilike("%" + term + "%"))
            results.append(Arenas.country.ilike("%" + term + "%"))
        query = query.filter(or_(*results))

    # apply specific field filters
    if min is None :
        min = float("-inf")
    if max is None :
        max = float("inf")

    if capacity is not None :
        query = query.filter(Arenas.capacity.between(min, max))
    if city is not None :
        query = query.filter(Arenas.city.ilike(city))
    if state is not None :
        query = query.filter(Arenas.city.ilike(state))
    if country is not None :
        query = query.filter(Arenas.country.ilike(country))
    if active is not None :
        query = query.filter(Arenas.active.is_(active))

    # sort the query
    if sort is not None :
        if getattr(Arenas, sort, None) is not None :
            query = query.order_by(getattr(Arenas, sort))

    # paginate
    total = query.count()
    if page is not None :
        query = paginate(query, page, page_size)

    # generate output
    result = arenas_schema.dump(query, many=True)
    return jsonify({"data": result, "total": total})

@app.route(URL_PREFIX + "arenas/<int:aid>")
def arena(aid) :
    """
    arenas table lookup by arena id
    
    :param aid: arena id
    :return: json file
    """
    query = db.session.query(Arenas).filter_by(aid = aid)
    try:
        result = arenas_schema.dump(query, many=True)
    except IndexError:
        return return_error(f"Invalid arena ID: {aid}")
    return jsonify({"data": result, "total": 1})

def paginate(query, page_num=1, page_size=DEFAULT_PAGE_SIZE) :
    """
    create a page-sized section of a query
    
    :param query: query to be paginated
    :param page_num: which page?
    :param page_size: how big are the pages?
    :return: json file
    """
    return query.paginate(page=page_num, per_page=page_size,
                          error_out=False).items

def return_error(msg, code=404) :
    """
    format a message into an html error response

    :param msg: error code message text
    :param code: html error code
    :return: Response object
    """
    response = Response(json.dumps({"Error: ": msg}), mimetype="application/json")
    response.error_code = code
    return response

if __name__ == "__main__" :
    app.run(debug=True)
