from flask_marshmallow import Marshmallow
from models import Players, Teams, Arenas
from marshmallow_sqlalchemy import SQLAlchemyAutoSchema

Marshmallow()


class PlayersSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = Players


class TeamsSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = Teams


class ArenasSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = Arenas


players_schema = PlayersSchema()
teams_schema = TeamsSchema()
arenas_schema = ArenasSchema()
