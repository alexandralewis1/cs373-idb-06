import unittest
import app

class Test(unittest.TestCase):
    
    def setUp(self):
        app.app.config["TESTING"] = True
        self.client = app.app.test_client()

    def test0(self):
        with self.client:
            response = self.client.get("/")
            self.assertEqual(response.status_code, 200)

    def test1(self): 
        with self.client:
            response = self.client.get("/v1/json/players")
            self.assertEqual(response.status_code, 200)
            data = response.json["data"]
            self.assertEqual(len(data), 491)

    def test2(self): 
        with self.client:
            response = self.client.get("/v1/json/teams")
            self.assertEqual(response.status_code, 200)
            data = response.json["data"]
            self.assertEqual(len(data), 30)

    def test3(self): 
        with self.client:
            response = self.client.get("/v1/json/arenas")
            self.assertEqual(response.status_code, 200)
            data = response.json["data"]
            self.assertEqual(len(data), 63)

    def test4(self): 
        with self.client:
            response = self.client.get("/v1/json/players/20000441")
            self.assertEqual(response.status_code, 200)
            data = response.json["data"]
            self.assertEqual(len(data), 1)

    def test5(self):
        with self.client:
            response = self.client.get("/v1/json/teams/1")
            self.assertEqual(response.status_code, 200)
            data = response.json["data"]
            self.assertEqual(len(data), 1)

    def test6(self):
        with self.client:
            response = self.client.get("/v1/json/arenas/1")
            self.assertEqual(response.status_code, 200)
            data = response.json["data"]
            self.assertEqual(len(data), 1)

    def test7(self):
        with self.client:
            response = self.client.get("/v1/json/players?offset=0&limit=10")
            self.assertEqual(response.status_code, 200)
            data = response.json["data"]
            self.assertEqual(len(data), 10)

    def test8(self):
        with self.client:
            response = self.client.get("/v1/json/teams?offset=0&limit=10")
            self.assertEqual(response.status_code, 200)
            data = response.json["data"]
            self.assertEqual(len(data), 10)

    def test9(self):
        with self.client:
            response = self.client.get("/v1/json/arenas?offset=0&limit=10")
            self.assertEqual(response.status_code, 200)
            data = response.json["data"]
            self.assertEqual(len(data), 10)

if __name__ == "__main__":
    unittest.main()
