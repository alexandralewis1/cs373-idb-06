import psycopg2
import json
import sqlite3


# conn = sqlite3.connect("sql/players.sql")
conn = psycopg2.connect(host='database-1.cn0cmdprv9xo.us-east-1.rds.amazonaws.com', database='nbadb', user='postgres', password='%C!c8TYnu%hm8*', port=5432)
cur = conn.cursor()

# read JSON data from file
# with open("raw/players.json") as f:
#     data = json.load(f)

# query = """ CREATE TABLE players (
#     id integer PRIMARY KEY,
#     tid integer,
#     teamshort text,
#     jersey integer,
#     position text,
#     first text,
#     last text,
#     height integer,
#     weight integer,
#     birthday text,
#     city text,
#     state text,
#     country text,
#     college text,
#     salary integer,
#     photo text,
#     experience integer
# )"""
# cur.execute(query)
# query = """ INSERT INTO players (
#     id,
#     tid,
#     teamshort,
#     jersey,
#     position,
#     first,
#     last,
#     height,
#     weight,
#     birthday,
#     city,
#     state,
#     country,
#     college,
#     salary,
#     photo,
#     experience
# ) VALUES (
#     ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?
# ) """
# i = 0
# for row in data :
#     cur.execute(query, (
#         data[i]["PlayerID"],
#         data[i]["TeamID"],
#         data[i]["Team"],
#         data[i]["Jersey"],
#         data[i]["Position"],
#         data[i]["FirstName"],
#         data[i]["LastName"],
#         data[i]["Height"],
#         data[i]["Weight"],
#         data[i]["BirthDate"],
#         data[i]["BirthCity"],
#         data[i]["BirthState"],
#         data[i]["BirthCountry"],
#         data[i]["College"],
#         data[i]["Salary"],
#         data[i]["PhotoUrl"],
#         data[i]["Experience"]))
#     i += 1

# read JSON data from file
# with open("raw/teams.json") as f:
#     data = json.load(f)

# query = """ CREATE TABLE IF NOT EXISTS teams (
#     tid integer PRIMARY KEY,
#     abbreviation text,
#     active boolean,
#     city text,
#     teamname text,
#     aid integer,
#     conference text,
#     division text,
#     primarycolor text,
#     secondarycolor text,
#     tertiarycolor text,
#     quaternarycolor text,
#     logo text,
#     nbadotcomteamid integer
# )"""
# cur.execute(query)
# query = """ INSERT INTO teams (
#     tid,
#     abbreviation,
#     active,
#     city,
#     teamname,
#     aid,
#     conference,
#     division,
#     primarycolor,
#     secondarycolor,
#     tertiarycolor,
#     quaternarycolor,
#     logo,
#     nbadotcomteamid
# ) VALUES (
#     %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s
# ) """

# for row in data:
#     cur.execute(
#         query,
#         (
#             row["TeamID"],
#             row["Key"],
#             row["Active"],
#             row["City"],
#             row["Name"],
#             row["StadiumID"],
#             row["Conference"],
#             row["Division"],
#             row["PrimaryColor"],
#             row["SecondaryColor"],
#             row["TertiaryColor"],
#             row["QuaternaryColor"],
#             row["WikipediaLogoUrl"],
#             row["NbaDotComTeamID"],
#         ),
#     )


# read JSON data from file
with open("raw/arenas.json") as f:
    data = json.load(f)

query = """ CREATE TABLE IF NOT EXISTS arenas (
    aid integer PRIMARY KEY,
    active boolean,
    arenaname text,
    address text,
    city text,
    state text,
    zip text,
    country text,
    capacity integer,
    lat integer,
    long integer
)"""
cur.execute(query)
query = """ INSERT INTO arenas (
    aid,
    active,
    arenaname,
    address,
    city,
    state,
    zip,
    country,
    capacity,
    lat,
    long
) VALUES (
    %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s
) """

for row in data:
    cur.execute(
        query,
        (
            row["StadiumID"],
            row["Active"],
            row["Name"],
            row["Address"],
            row["City"],
            row["State"],
            row["Zip"],
            row["Country"],
            row["Capacity"],
            row["GeoLat"],
            row["GeoLong"],
        ),
    )
conn.commit()
conn.close()


# source: https://thedbadmin.com/insert-json-data-file-into-postgresql-using-python-script/
# source: https://www.youtube.com/watch?v=HX-ChCQfJEo
