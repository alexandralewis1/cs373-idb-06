import requests
from bs4 import BeautifulSoup
import psycopg2
import flask_sqlalchemy

# Send a request to the URL
url = "https://www.basketball-reference.com/friv/twitter.html"
response = requests.get(url)

# Parse the HTML content using BeautifulSoup
soup = BeautifulSoup(response.content, "html.parser")

# Find the table containing the data
table = soup.find_all("table")[0]

# Create an empty dictionary to store the player and Twitter handle
player_dict = {}

# Iterate through each row in the table
for row in table.find_all("tr")[1:]:
    # Extract the player's name and Twitter handle from the table cells
    cells = row.find_all("td")
    player_name = cells[0].get_text()
    twitter_handle = cells[1].find("a")["href"]

    # Add the player and Twitter handle to the dictionary
    player_dict[player_name] = twitter_handle

# Print the dictionary
print(player_dict)

conn = psycopg2.connect(host='database-1.cn0cmdprv9xo.us-east-1.rds.amazonaws.com', database='nbadb', user='postgres', password='%C!c8TYnu%hm8*', port=5432)
cur = conn.cursor()

cur.execute("ALTER TABLE players ADD twitter text")

# Loop through the dictionary and check for matches in the database
for name, url in player_dict.items():
    print(name)
    if (name == "Nenê"):
        first_name, last_name = "Nenê", "last"
    else:
        first_name, last_name = name.split(" ", 1)

    # Execute a SELECT query to search for a matching name in the database
    cur.execute(
        "SELECT * FROM players WHERE first = %s AND last = %s",
        (first_name, last_name)
    )

    # If a match is found, update the Twitter URL in the database
    if cur.fetchone() is not None:
        cur.execute(
            "UPDATE players SET twitter = %s WHERE first = %s AND last = %s",
            (url, first_name, last_name)
        )

# Commit the changes to the database
conn.commit()

# Close the cursor and database connection
cur.close()
conn.close()
