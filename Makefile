SHELL := bash
.ONESHELL:
# .DEFAULT_GOAL := clean

clean:
	rm -rf frontend/dist
	rm -rf frontend/node_modules

install-backend:
	( \
			cd backend; \
			pip3 install virtualenv; \
			virtualenv env -p python3; \
			source env/bin/activate; \
			pip3 install flask; \
			pip3 freeze; \
			pip3 install -U flask-cors; \
			pip3 install flask-marshmallow; \
			pip3 install marshmallow-sqlalchemy; \
			pip3 install flask_sqlalchemy; \
			pip install psycopg2; \
	)

activate-backend:
	( \
			cd backend/src; \
			source ../env/bin/activate; \
			FLASK_APP=app.py; \
			flask run --host=0.0.0.0 --port=4000; \
	)

format-backend:
	( \
			cd backend; \
			pip3 install virtualenv; \
			virtualenv env -p python3; \
			source env/bin/activate; \
			pip3 install black; \
			black src; \
	)

format-lint: 
	make format-backend; \
	cd frontend && npm run format && npm run lint; \


deploy-backend:
		( \
			cd backend; \
			pip3 install virtualenv; \
			virtualenv env -p python3.9; \
			source env/bin/activate; \
			pip3 install flask; \
			pip3 install flask-cors; \
			pip3 install flask-marshmallow; \
			pip3 install marshmallow-sqlalchemy; \
			pip3 install flask-sqlalchemy; \
			pip3 install psycopg2; \
			pip3 install psycopg2-binary; \
			pip3 install zappa; \
			cd src; \
			zappa update prod; \
	)

selenium:
	cd frontend; \
    pip3 install selenium; \
    pip install webdriver-manager; \
    wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -; \
    sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google-chrome.list'; \
    yum -y update; \
    yum install -y google-chrome-stable; \
    yum install -yqq unzip; \
    wget -O /tmp/chromedriver.zip http://chromedriver.storage.googleapis.com/`curl -sS chromedriver.storage.googleapis.com/LATEST_RELEASE`/chromedriver_linux64.zip; \
    unzip /tmp/chromedriver.zip chromedriver -d /usr/local/bin/; \
    python3 guitests.py; \
