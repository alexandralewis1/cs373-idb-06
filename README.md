# NBADB

### Website Link

https://www.nbadb.me

### GitLab Pipelines

https://gitlab.com/alexandralewis1/cs373-idb-06/-/pipelines


## Group Info

| Name               | GitLabID         | EID     |
|:------------------ | ---------------- | ------- |
| Aditya Agrawal     | @DaSniper12      | aa87823 |
| Kelly Thai         | @kellythai02     | kxt75   |
| Alexandra Lewis    | @alexandralewis1 | ael2924 |
| Ayush Patel        | @AyushPatel101   | ap55837 |
| Mason Wilderom     | @wilderom        | mw39432 |

### Project Leaders

#### Responsibilities

* Organize and direct group meetings
* Make sure everyone is on track
* Help out anyone if their running into any problems

| Phase | Project Leader  |
|:----- | --------------- |
| 1     | Aditya Agrawal  |
| 2     | Ayush Patel     |
| 3     | Alexandra Lewis |
| 4     | Kelly Thai      |


## Phase 1

### Git SHA
c6d8e2b0ac9e44e258c23c859b6658c46c259f16

### Completion Times

| Name               | Est. Completion Time (hrs) | Real Completion Time (hrs) |
|:------------------ | -------------------------- | -------------------------- |
| Aditya Agrawal     | 5                          | 5                          |
| Kelly Thai         | 10                         | 15                         |
| Alexandra Lewis    | 7                          | 12                         |
| Ayush Patel        | 15                         | 10                         |
| Mason Wilderom     | 9                          | 7                         |


## Phase 2

### Git SHA
28eff0755a844d53a06b30a1f4f97779d497919c

### Completion Times

| Name               | Est. Completion Time (hrs) | Real Completion Time (hrs) |
|:------------------ | -------------------------- | -------------------------- |
| Aditya Agrawal     | 20                         | 30                         |
| Kelly Thai         | 25                         | 38                         |
| Alexandra Lewis    | 32                         | 50                         |
| Ayush Patel        | 35                         | 42                         |
| Mason Wilderom     | 20                         | 21                         |

## Phase 3

### Git SHA
b44e6adc6627c107f17c9921461aa902098f2de0

### Completion Times

| Name               | Est. Completion Time (hrs) | Real Completion Time (hrs) |
|:------------------ | -------------------------- | -------------------------- |
| Aditya Agrawal     | 20                         | 27                         |
| Kelly Thai         | 25                         | 30                         |
| Alexandra Lewis    | 22                         | 43                         |
| Ayush Patel        | 23                         | 32                         |
| Mason Wilderom     | 25                         | 30                         |

## Phase 4

### Git SHA
352c06273281e89032022c3bd834c6688cc11bee

### Completion Times

| Name               | Est. Completion Time (hrs) | Real Completion Time (hrs) |
|:------------------ | -------------------------- | -------------------------- |
| Aditya Agrawal     | 14                         | 13                         |
| Kelly Thai         | 12                         | 12                         |
| Alexandra Lewis    | 10                         | 12                         |
| Ayush Patel        | 10                         | 13                         |
| Mason Wilderom     | 12                         | 12                         |

## Comments